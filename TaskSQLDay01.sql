CREATE DATABASE db_Penerbit

USE db_Penerbit

CREATE TABLE tbl_Pengarang 
(
ID INT PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang VARCHAR(7) NOT NULL,
Nama VARCHAR(30) NOT NULL,
Alamat VARCHAR(80) NOT NULL,
Kota VARCHAR(15) NOT NULL,
Kelamin VARCHAR(1) NOT NULL
)


CREATE TABLE tbl_Gaji
(
ID INT PRIMARY KEY IDENTITY(1,1),
Kd_Pengarang VARCHAR(7) NOT NULL,
Nama VARCHAR(30) NOT NULL,
Gaji DECIMAL(18,4) NOT NULL
)


INSERT INTO tbl_Pengarang
VALUES
('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
('P0002','Rian','Jl. Solo 123','Yogya','P'),
('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
('P0004','Siti','Jl. Durian 15','Solo','W'),
('P0005','Amir','Jl. Gajah 33','Kudus','P'),
('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
('P0007','Jaja','Jl. Singa 7','Bandung','P'),
('P0008','Saman','Jl. Naga 12','Yogya','P'),
('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

INSERT INTO tbl_Gaji
VALUES
('P0002','Rian',600000),
('P0005','Amir',700000),
('P0004','Siti',500000),
('P0003','Suwadi',1000000),
('P0010', 'Fatmawati',600000),
('P0008','Saman', 750000)

select * from tbl_Pengarang
select * from tbl_Gaji

--- No 1
SELECT COUNT(id)[Jumlah Pengarang], Nama from tbl_Pengarang group by Nama
--- No 2
SELECT COUNT(Kelamin)total_jenis_kelamin, Kelamin from tbl_Pengarang GROUP BY Kelamin
--- No 3
SELECT COUNT(Kota)[Total Kota], kota from tbl_Pengarang GROUP BY Kota
--- No 4
SELECT COUNT(Kota)[Total Kota], kota from tbl_Pengarang GROUP BY Kota HAVING COUNT(Kota)>1
--- No 5
SELECT Kd_Pengarang from tbl_Pengarang ORDER BY Kd_Pengarang DESC
--atau
SELECT top 1 Kd_Pengarang as tertinggi, Kd_Pengarang as terendah from tbl_Pengarang ORDER BY tertinggi DESC, terendah ASC
--- No 6
SELECT MAX(Gaji)[Gaji Tertinggi], MIN(Gaji)[Gaji Terendah] from tbl_Gaji
--- No 7
SELECT * from tbl_Gaji WHERE Gaji>600000
--- No 8
SELECT SUM(Gaji)[Total gaji yang dikeluarkan] from tbl_Gaji 
--- No 9
SELECT pengarang.Kota, SUM(gaji.Gaji)[Total Gaji]
FROM tbl_Pengarang AS pengarang
JOIN tbl_Gaji AS gaji ON pengarang.Kd_Pengarang = gaji.Kd_Pengarang
GROUP BY pengarang.Kota
--- No 10
SELECT Nama, Kd_Pengarang FROM tbl_Pengarang WHERE Kd_Pengarang BETWEEN 'P0003' AND 'P0006'
--atau
SELECT Nama, Kd_Pengarang FROM tbl_Pengarang WHERE Kd_Pengarang >= 'P0002' AND Kd_Pengarang<= 'P0005'

--- No 11
SELECT * FROM tbl_Pengarang WHERE Kota IN ('Yogya','Solo','Magelang') 

--- No 12
SELECT * FROM tbl_Pengarang WHERE NOT Kota = ('Yogya')
--atau
SELECT * FROM tbl_Pengarang WHERE Kota != 'Yogya'
--atau
SELECT * FROM tbl_Pengarang WHERE  Kota <> 'Yogya'

--- No 13
--a
SELECT * FROM tbl_Pengarang WHERE nama LIKE 'a%'
--b
SELECT * FROM tbl_Pengarang WHERE nama LIKE '%i'
--c
SELECT * FROM tbl_Pengarang WHERE nama LIKE '__a%'
--d
SELECT * FROM tbl_Pengarang WHERE NOT nama  LIKE  '%n'
--e 
SELECT * FROM tbl_Pengarang WHERE NOT Kd_Pengarang LIKE '%4' AND Nama LIKE '%i'

--- No 14
SELECT pengarang.Kd_Pengarang, pengarang.Nama, pengarang.Alamat, pengarang.Kota, gaji.Gaji
FROM tbl_Pengarang AS pengarang 
JOIN tbl_Gaji AS gaji ON pengarang.Kd_Pengarang = gaji.Kd_Pengarang

SELECT *
FROM tbl_Pengarang AS pengarang 
JOIN tbl_Gaji AS gaji ON pengarang.Kd_Pengarang = gaji.Kd_Pengarang

--- No 15
SELECT pengarang.Kota, gaji.Gaji
FROM tbl_Pengarang AS pengarang 
JOIN tbl_Gaji AS gaji ON pengarang.Kd_Pengarang = gaji.Kd_Pengarang
WHERE gaji.Gaji < 1000000 

--- No 16
ALTER TABLE tbl_Pengarang ALTER COLUMN Kelamin VARCHAR(10)

--- No 17
ALTER TABLE tbl_Pengarang ADD Gelar VARCHAR(12)

--- No 18
UPDATE tbl_Pengarang SET Alamat = 'Jl. Cendrawasih 65', Kota ='Pekanbaru' WHERE nama='Rian' 

--- No 19
CREATE VIEW vwPengarang 
AS 
SELECT pengarang.Kd_Pengarang, pengarang.Nama, pengarang.Kota, gaji.Gaji 
from tbl_Pengarang as pengarang
join tbl_gaji as gaji on pengarang.Kd_Pengarang = gaji.Kd_Pengarang


select *, ROW_NUMBER() over(order by Nama) as [nomor urut] from vwPengarang


sp_columns 'vwPengarang'
