﻿using System;
using System.Collections.Generic;

namespace logicDay06
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //list();
            //contohClass();
            //listClass();
            //lissAdd();
            lissRemove();
            Console.ReadKey();
        }


        static void lissRemove() 
        {
            Console.WriteLine("==================== Menambahkan data pada LIST ====================\n");
            List<User> user = new List<User>()
            {
                new User(){Nama = "Isni Dwitiniwinibiti", Umur = 22},
                new User(){Nama = "Alfi Azizi", Umur = 23},
                new User(){Nama = "Astika Fujianti Rahayu", Umur=23}
            };

            user.Add(new User() { Nama = "Ahmad Kahirul Anwar", Umur = 24 });
            user.RemoveAt(2);
            for (int i = 0; i < user.Count; i++)
            {
                Console.WriteLine("\t" + user[i].Nama + " sudah berumur " + user[i].Umur + " tahun.");

            }
        }

        static void lissAdd()
        {
            Console.WriteLine("==================== Menambahkan data pada LIST ====================\n");
            List<User> user = new List<User>()
            {
                new User(){Nama = "Isni Dwitiniwinibiti", Umur = 22},
                new User(){Nama = "Alfi Azizi", Umur = 23},
                new User(){Nama = "Astika Fujianti Rahayu", Umur=23}
            };

            user.Add(new User() { Nama = "Ahmad Kahirul Anwar", Umur = 24 });

            for (int i = 0; i < user.Count; i++)
                Console.WriteLine("\t" + user[i].Nama + " sudah berumur " + user[i].Umur + " tahun.");
 
        }

        static void listClass()
        {
            List<User> user = new List<User>()
            {
                new User(){Nama = "Isni Dwitiniwinibiti", Umur = 22},
                new User(){Nama = "Alfi Azizi", Umur = 23},
                new User(){Nama = "Astika Fujianti Rahayu", Umur=23}
            };

            for (int i = 0; i < user.Count; i++)
            {
                Console.WriteLine("\t" + user[i].Nama + " sudah berumur " + user[i].Umur + " tahun.");
                
            }

        }

        static void contohClass()
        {
            //Mobil mobil = new Mobil();
            Mobil mobil = new Mobil("RI SATU");

            mobil.percepat();
            mobil.maju();
            mobil.isiBensin(12);

            string platno = mobil.getPlatNo();
            Console.WriteLine($"Plat Nomor\t: {platno}");
            Console.WriteLine($"Bensin\t\t: {mobil.bensin}");
            Console.WriteLine("Kecepatan\t: " + mobil.kecepatan);
            Console.WriteLine("Posisi\t\t: " + mobil.posisi);
        }

        static void list()
        {
            Console.WriteLine("\t\t=========================== LIST ===========================");

            List<string> list = new List<string>()
            {
                "Astika","Marchelino","Alfi Azizi", "Firdha" 
            };

            Console.WriteLine("\t\t"+string.Join(", ", list));

        }
    }
}
