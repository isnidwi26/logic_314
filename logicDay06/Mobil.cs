﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logicDay06
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        private string nama;
        private string platno;


        public Mobil()
        {

        }

        //constructor
        public Mobil (string _platno)
        {
            platno = _platno;
        }

        //METHOD
        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;


        }

        public string getPlatNo()
        {
            return platno;
        }
    }
}
