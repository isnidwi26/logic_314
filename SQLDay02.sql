	----CAST
select cast(10  as decimal(18,4))
select cast('10' as int)
select cast(10.65 as int)
select cast('2023-03-16' as datetime)
select GETDATE(), GETUTCDATE()
select DAY(GETDATE()), MONTH(getdate()), YEAR(GETDATE())

	-----Convert
select convert(decimal(18,4), 10)
select CONVERT(int, '10')
select CONVERT(int, 10.65)
select CONVERT(datetime, '2023-03-26')

	-----DATE TIME
select DATEADD(year, 2,GETDATE()), DATEADD(MONTH,3,GETDATE()), DATEADD(DAY, 5, GETDATE())

-- datediff() untuk mengembalikkan perbedaan antara dua tanggal
select DATEDIFF(DAY, '2023-03-16','2023-03-25'), 
	   DATEDIFF(MONTH,'2023-03-16','2023-06-16'),
	   DATEDIFF(YEAR,'2023-03-16','2030-03-16')

				----SUB QUERY SQL SERVER----

insert into [dbo].[mahasiswa]
select name, address, email, panjang from mahasiswa

select name, address, email, panjang from mahasiswa
where panjang = (select MAX(panjang) from mahasiswa)

				----VIEW SQL SERVER----
create view vwMahasiswa
as
select * from mahasiswa

				----DATABASE INDEX----
--membuat indeks data yang memudahkan dalam pencarian yang datanya sudah banyak
--create index
create index index_name
on mahasiswa(name)

create index index_address_email
on mahasiswa(address, email)
--create unique index
create unique index uniqueindex_address
on mahasiswa(address)
--drop index
drop index index_address_email on mahasiswa
--drop unique index
drop index uniqueindex_address on mahasiswa


				----PRIMARY KEY DAN UNIQUE KEY----
--add primary key
alter table mahasiswa add constraint pk_address primary key(address)

--drop primary key
alter table mahasiswa drop constraint PK__mahasisw__3213E83F647C2FD6

alter table mahasiswa drop constraint PK_address
alter table mahasiswa add constraint pk_id_address primary key(id,address)

--add unique key constraint
alter table mahasiswa add constraint unique_address unique(address)
--drop unique key constraint
alter table mahasiswa drop constraint unique_address

alter table mahasiswa add constraint unique_panjang unique(panjang)

--tes apakah unique key bisa null atau tidak
update mahasiswa set panjang=null where id=7
update mahasiswa set panjang=83 where id=7




select * from mahasiswa
select * from biodata




