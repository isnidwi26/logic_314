﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;

namespace simulationLogic
{
    class Program
    {
        static void Main(string[] args)
        {
            /*SIMULASI LOGIC*/
            //cekKata();
            //invoicePenjualan();
            //geser();
            //keranjangBuah();
            //pembagianBaju();
            //pembulatanNilai();
            //deretGenapdanGanjil();
            //recursiveDigit();
            //poinPulsa();
            //selisih();
            //kalimatPangram();
            //theOneNumber();

            /*CONTOH SOAL LAIN*/
            //perulanganTanpaPerulangan();

            /*SIMULASI ASTRA*/
            //bilPrima();
            //menghaspuNodeAngkaGenap();
            angkaPrima();

            /*SOAL HACKERRANK*/
            //sos();
            //separateTheNumbers();



        }


        //SIMULASI LOGIC
        static void cekKata()
        {
            //SOAL NO 1
            Console.WriteLine("\t========Menghitung Kata========");
            Console.Write("\tMasukkam kalimat tanpa spasi: ");
            string kata = Console.ReadLine();

            int hitung = 0;

            foreach (char hitKata in kata)
            {
                if (char.IsUpper(hitKata))
                {
                    hitung++;
                }
            }

            /*char[] hitKata = kata.ToCharArray();
            for (int i = 0; i < hitKata.Length; i++)
            {
                if (kata[i] == kata[0] || char.IsUpper(kata[i]))
                {
                    hitung++;
                }
            }*/

            Console.WriteLine("\tJumlah kata pada kalimat diatas sebanyak: " + hitung);
        }

        static void invoicePenjualan()
        {
            //SOAL NO 2
            Console.WriteLine("\t======== Invoice Penjualan ========");
            Console.Write("\tStart Penjualan: ");
            int awal = int.Parse(Console.ReadLine());
            Console.Write("\tAkhir Penjualan: ");
            int akhir = int.Parse(Console.ReadLine());

            for (int i = awal; i <= akhir; i++)
            {
                Console.WriteLine("XA-" + DateTime.Now.ToString("ddMMyyyy") + "-" + i.ToString().PadLeft(5, '0'));
            }

        }

        static void keranjangBuah()
        {
            //SOAL NO 3
            int[] buah = new int[3];
            int jumlah = 0;
            for (int i = 0; i < buah.Length; i++)
            {
                Console.Write("\tKeranjang " + (i + 1) + ": ");
                buah[i] = int.Parse(Console.ReadLine());
                jumlah += buah[i];
            }
            Console.Write("\tPilih keranjang yang akan di bawa ke pasar: ");
            int indeksKeranjang = int.Parse(Console.ReadLine());

            jumlah = jumlah - buah[indeksKeranjang - 1];

            Console.WriteLine("\tSisa buah sebanyak: " + jumlah);
        }

        static void pembagianBaju()
        {
            //SOAL NO 4
            int jumlah, jumlahBajuLaki = 0, jumlahBajuWanita = 0, jumlahBajuAnak = 0, jumlahBajuBayi = 0, total = 0, jumlahWanita = 0;
            Console.WriteLine("\tPilihlah baju yang akan di bagi:");
            Console.WriteLine("\t1. Laki Dewasa");
            Console.WriteLine("\t2. Wanita Dewasa");
            Console.WriteLine("\t3. Anak-anak");
            Console.WriteLine("\t4. Bayi");
        lanjut:
            Console.Write("\tPilih kategori baju: ");
            int pilih = int.Parse(Console.ReadLine());
            char ulang = ' ';

            switch (pilih)
            {
                case 1:
                    Console.Write("\tJumlah baju Laki-laki dewasa: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuLaki += jumlah;
                    //total += jumlahBajuLaki;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 2:
                    Console.Write("\tJumlah baju wanita dewasa: ");
                    jumlahWanita = int.Parse(Console.ReadLine());
                    jumlahBajuWanita += jumlahWanita * 2;
                    //total += jumlahBajuWanita;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 3:
                    Console.Write("\tJumlah baju anak-anak: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuAnak += jumlah * 3;
                    //total += jumlahBajuAnak;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                case 4:
                    Console.Write("\tJumlah baju bayi: ");
                    jumlah = int.Parse(Console.ReadLine());
                    jumlahBajuBayi += jumlah * 5;
                    //total += jumlahBajuBayi;
                    Console.Write("\tApakah kamu akan input lagi?(y/n): ");
                    ulang = char.Parse(Console.ReadLine());
                    if (ulang == 'y')
                        goto lanjut;
                    break;
                default:
                    Console.WriteLine("\tTidak ada pilihan");
                    goto lanjut;
            }
            total = jumlahBajuAnak + jumlahBajuBayi + jumlahBajuLaki + jumlahBajuWanita;

            if (total % 2 == 1 && total > 10)
                total += jumlahWanita;

            Console.WriteLine("\tTotal baju: " + total);


        }

        static void pembulatanNilai()
        {
            //SOAL NO 5
            Console.WriteLine("\t===== Pembulatan Nilai =====");
            Console.Write("\tMasukkan nilai: ");
            int nilaiAsal = int.Parse(Console.ReadLine());

            double nilaiakhir = 0, nilaiUbah = 0, temp;

            temp = Math.Round((nilaiAsal / 5d), MidpointRounding.ToPositiveInfinity);
            nilaiUbah = temp * 5;

            if (nilaiUbah - nilaiAsal < 3)
                nilaiakhir = nilaiUbah;
            else
                nilaiakhir = nilaiAsal;
            Console.WriteLine("Nilai akhir pembulatan adalah: " + nilaiakhir);

        }

        static void kalimatPangram()
        {
            //SOAL NO 6
            Console.WriteLine("\t===== Pangrams =====");
            Console.Write("\tMasukkan kalimat: ");
            string kal = Console.ReadLine().ToLower();

            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            bool pangram = true;
            foreach (char kat in alfabet)
            {
                if (!kal.Contains(kat))
                {
                    pangram = false;
                    break;
                }
            }
            if (pangram)
            {
                Console.WriteLine("\tKalimat ini adalah kalimat pangram");
            }
            else
            {
                Console.WriteLine("\tKalimat ini bukan pangram");
            }
        }

        static void deretGenapdanGanjil()
        {
            //SOAL NO 7
            Console.WriteLine("\t=========== Deret Genap, Ganjil, dan Fibonaci ===========");
            Console.Write("\tMasukkan panjang deret himpunan: ");
            int panjang = int.Parse(Console.ReadLine());
            int[] fib = new int[panjang];
            int genap = 2, ganjil = 1, totalgenap = 0, totalganjil = 0, totalfib = 0;
            Console.Write("\t");
            for (int i = 0; i < panjang; i++)
            {
                if (i <= 1)
                    fib[i] = 1;
                else
                    fib[i] = fib[i - 2] + fib[i - 1];
                Console.Write(fib[i] + " ");
                totalfib += fib[i];
            }
            Console.WriteLine("\n\tTotal Fibonaci: " + totalfib);
            Console.WriteLine("\tRata-rata Fibonaci: " + Math.Round(Convert.ToDouble(totalfib) / Convert.ToDouble(panjang), 1));

            Console.WriteLine("\n\tGenap: ");
            Console.Write("\t");
            for (int i = 0; i < panjang; i++)
            {
                totalgenap += genap;
                Console.Write(genap + " ");
                genap += 2;

            }
            Console.WriteLine("\n\tTotal Genap: " + totalgenap);
            Console.WriteLine("\tRata-rata Genap: " + totalgenap / 10d);

            Console.WriteLine("\n\tGanjil: ");
            Console.Write("\t");
            for (int i = 0; i < panjang; i++)
            {
                totalganjil += ganjil;
                Console.Write(ganjil + " ");
                ganjil += 2;
            }
            Console.WriteLine("\n\tTotal Ganjil: " + totalganjil);
            Console.WriteLine("\tRata-rata Ganjil: " + totalganjil / 10d);
        }

        static void recursiveDigit()
        {
            //SOAL NO 8
            Console.WriteLine("\t===== Recursive Digit =====");
            Console.Write("\tMasukkan angka: ");
            //int[] angka =  Array.ConvertAll(Console.ReadLine().Split(""), int.Parse);
            char[] angka = Console.ReadLine().ToCharArray();


            Console.Write("\tJumlah Pengulangan: ");
            int pengulangan = int.Parse(Console.ReadLine());

            int[] convAngka = angka.Select(c => Convert.ToInt32(c.ToString())).ToArray();

            int n = angka.Length;

            int jumlahAngka = 0;

            for (int j = 0; j < n; j++)
            {
                jumlahAngka += convAngka[j];
            }


            int total, cek = 0;
            cek = jumlahAngka * pengulangan;
            char[] jumAngka = cek.ToString().ToCharArray();

        ulang:
            int[] convJumAngka = jumAngka.Select(c => Convert.ToInt32(c.ToString())).ToArray();

            if (convAngka.Length >= 2)
            {
                total = 0;
                for (int i = 0; i < convJumAngka.Length; i++)
                {
                    //Console.WriteLine(convJumAngka[i]);
                    total += convJumAngka[i];
                }
                if (total > 9)
                {
                    jumAngka = total.ToString().ToCharArray();
                    goto ulang;
                }
                for (int i = 0; i < pengulangan; i++)
                {
                    Console.Write(string.Join("+", convAngka));
                }
                Console.WriteLine("\tHasil Recursive : " + total);
            }
            else
                Console.WriteLine("\tHasil Recursive : " + convJumAngka);
        }

        static void poinPulsa()
        {
            //SOAL NO 9
            Console.WriteLine("\t====== Poin Pulsa ======");
            Console.Write("\tMau beli pulsa berapa qaqa? ");
            int pulsa = int.Parse(Console.ReadLine());
            int poin1, poin2;

            if (pulsa <= 10000)
            {
                Console.WriteLine("\t0 = 0 Point");
            }
            else if (pulsa <= 30000)
            {
                poin1 = (pulsa / 10000) / 1000;
                Console.WriteLine("\t0 + " + poin1 + "= " + poin1 + " Point");
            }
            else
            {
                poin1 = 20000 / 1000;
                poin2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine("\t0 + " + poin1 + "+ " + poin2 + " = " + (poin1 + poin2) + " Point");
            }

            /*if (pulsa >= 10000)
            {
                sisapulsa = pulsa - 10000;
                poin = 0;

                if (sisapulsa >= 10000)
                {
                    if(sisapulsa > 30000)

                    sisapulsa /= 1000;
                    poin += sisapulsa;
                }
            }*/
        }

        static void selisih()
        {
            //SOAL NO 10
            Console.WriteLine("\t========== Selisih angka ==========");
            Console.Write("\tMasukan selisih angka: ");
            int selisih = int.Parse(Console.ReadLine());

            int hitung = 0;

            int[] angka = { 1, 5, 3, 4, 2 };

            for (int i = 0; i < angka.Length; i++)
            {
                for (int j = 0; j < angka.Length; j++)
                {
                    if (angka[i] - angka[j] == selisih)
                    {
                        hitung++;
                    }
                }
            }

            Console.WriteLine("\tBanyaknya selisih: " + hitung);

        }

        static void theOneNumber()
        {
            Console.WriteLine("\t===== The One Number =====");
            Console.Write("\tJumlah output: ");
            int jumlahOutput = int.Parse(Console.ReadLine());
            int mulai = 100;
            int increment = 0;
            do
            {
                //CONVERT ANGKA AWAL MENJADI CHAR ARRAY
                char[] num = mulai.ToString().ToCharArray();
            ngulang:
                //CONVERT CHAR ARRAY MENJADI INTEGER ARRAY
                int[] convAngka = num.Select(d => Convert.ToInt32(d.ToString())).ToArray();
                //MENJUMLAHKAN PERDIGIT PADA ANGKA DARI ARRAY INTEGER
                double hitung = 0;
                for (int i = 0; i < convAngka.Length; i++)
                {
                    hitung += Math.Pow(convAngka[i], 2);
                    //Console.WriteLine(hitung);
                }
                //KONDISI JIKA PENJUMLAHAN PERDIGIT DARI ANGKA LEBIH DARI 10
                if (hitung > 10)
                {
                    //MEMINDAHKAN NILAI PADA VARIABEL HITUNG KE CHAR ARRAY NUM DAN LANJUT KE GOTO NGULANG UNTUK MENGHITUNG PERDIGIT LAGI
                    num = hitung.ToString().ToCharArray();
                    goto ngulang;
                }
                //KONDISI NILAI HITUNGNYA KURANG DARI SAMA DENGAN 10
                else
                {
                    //KONDISI UNTUK MENAMPILKAN YANG HASILNYA THE ONE NUMBER SAJA
                    if (hitung == 1 || hitung == 10)
                    {
                        Console.WriteLine("\t" + mulai + " is the one number");
                        increment++;
                    }
                    //KALAU MAU LIAT YANG BUKAN THE ONE NUMBER KASIH ELSE SAMA STATEMENT NYA AJA TAPI NANTI OUTPUTNYA BAKAL BERCAMPUR SAMA SEMUA KONDISI 
                    //PUSING GA? SAMA SAYA JUGA
                }
                mulai++;
            } while (increment < jumlahOutput && mulai <= 1000);
        }



        //LATIHAN SOAL LAIN
        static void perulanganTanpaPerulangan()
        {
            Console.WriteLine("\t===== Perulangan tanpa perulangan =====");
            Console.Write("\tMasukkan angka: ");
            int input = int.Parse(Console.ReadLine());

            int start = 1;
            PerulanganDESC(input);
            PerulanganASC(input, start);


        }

        static int PerulanganASC(int input, int start)
        {

            if (start == input)
            {
                Console.WriteLine(start);
                return start;
            }
            Console.WriteLine(start);
            return PerulanganASC(input, start + 1);
        }

        static int PerulanganDESC(int input)
        {

            if (input == 0)
            {
                return input;
            }
            Console.WriteLine(input);
            return PerulanganDESC(input - 1);
        }



        //SIMULASI ASTRA
        static void bilPrima()
        {
            Console.WriteLine("\t======Bilangan Prima======");
            Console.Write("\tMasukkan jumlah bilangan: ");
            int bil = int.Parse(Console.ReadLine());
            bool prima = true;

            for (int i = 2; i <= bil; i++)
            {
                for (int j = 2; j < i; j++)
                {
                    if ((i % j) == 0)
                    {
                        prima = false;
                        break;
                    }
                }
                if (prima)
                    Console.WriteLine(i);
                prima = true;

            }

            //KISI-KISI ASTRA
            /*for (int j = 2; j < bil; j++)
            {
                if ((bil % j) == 0)
                {
                    prima = false;
                    break;
                }
            }
            if (prima)
                Console.WriteLine("\t bilangan " + bil + " adalah bilangan prima");
            else
                Console.WriteLine("\t bilangan " + bil + " bukan bilangan prima");*/


        }

        static void menghaspuNodeAngkaGenap()
        {
            List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            for (int i = 0; i < list.Count(); i++)
            {
                if (list[i] % 2 == 0)
                    list.RemoveAt(i);
            }
            Console.WriteLine(string.Join(", ", list));

            //menambahkan data di sesuai indeks
            list.Insert(1, 20);
            //mencari indeks dari value yang ada
            list.IndexOf(9);

        }

        static void angkaPrima()
        {
            Console.WriteLine("\t======Bilangan Prima======");
            Console.Write("\tMasukkan jumlah bilangan: ");
            int bil = int.Parse(Console.ReadLine());
            bool prima = true;
            int tamp = 0;
            if (bil >= 2)
            {
                for (int j = 2; j < bil; j++)
                {
                    if (bil % j == 0)
                    {
                        tamp = j;
                        prima = false;
                        break;

                    }
                }
                if (prima)
                    Console.WriteLine("\t1");
                else
                    Console.WriteLine("\t" + tamp);
            }
            else
            {
                Console.WriteLine("\tInputan bukan bilangan prima");
            }


        }



        //SOAL HACKERRANK
        static void geser()
        {
            Console.Write("inputan: ");
            string s = Console.ReadLine();
            Console.Write("jumlah rotasi: ");
            int k = int.Parse(Console.ReadLine());


            string alfabet = "abcdefghijklmnopqrstuvwxyz";
            string rotAlfabet = alfabet.Substring(k) + alfabet.Substring(0, k);

            Console.WriteLine(rotAlfabet);

            char[] arrAlfabet = alfabet.ToCharArray();
            char[] arrRotAlfabet = rotAlfabet.ToCharArray();
            char[] arr = s.ToCharArray();

            /*string rot = "";
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr.Contains(alfabet[i]))
                {
                    if (char.IsUpper(arr[i]))
                        rot += arrRotAlfabet[arr.IndexOf(arrAlfabet), char.ToLower().ToString()].ToString().ToUpper();
                    else
                        rot += arrRotAlfabet[arr.IndexOf(arrAlfabet), char.ToLower().ToString()].ToString().ToLower();
                }
                else
                { }
            }*/



            /*char rot = ' ';

            char[] geser = s.ToCharArray();

            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < geser.Length; j++)
                {
                    if (geser[j] == )
                }
            }
            Console.WriteLine(string.Join("", geser));*/






            /*char rot = ' ';

            char[] geser = s.ToCharArray();

            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < geser.Length; j++)
                {
                    if (j < 1)
                    {
                        rot = geser[j];
                        geser[j] = geser[j + 1];
                    }
                    else if (j >= 1 && j < geser.Length - 1)
                        geser[j] = geser[j + 1];
                    else
                        geser[geser.Length - 1] = rot;
                }
            }
            Console.WriteLine(string.Join("", geser));



            string[] input = s.ToLower().Split("-");
            string rot = "";

            for (int l = 0; l < k; k++)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    for (int j = 0; j < input[i].Length; j++)
                    {
                        if (j < 1)
                        {
                            rot = input[j];
                            input[j] = input[j + 1];
                        }
                        else if (j >= 1 && j < input.Length - 1)
                            input[j] = input[j + 1];
                        else
                            input[input.Length - 1] = rot;
                    }
                }
            }

            Console.WriteLine(string.Join("-", input));*/


        }

        static void sos()
        {
            Console.WriteLine("\t=============== Cek SOS ===============");
            Console.Write("\tMasukan kode : ");
            string s = Console.ReadLine();

            char[] sos = s.ToUpper().ToCharArray();
            int hitungbenar = 0, hitungsalah = 0;

            if (sos.Length % 3 == 0)
            {
                for (int i = 0; i < sos.Length; i += 3)
                {
                    if (sos[i] != 'S' || sos[i + 1] != 'O' || sos[i + 2] != 'S')
                    {
                        hitungsalah++;

                    }
                    else
                    {
                        hitungbenar++;
                    }
                }
            }

            Console.WriteLine("\tJumlah salah: " + hitungsalah);
        }

    }




}
