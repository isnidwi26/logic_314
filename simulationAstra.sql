-- KISI-KISI

create database db_nilai
use db_nilai

create table assignment
(
id int primary key identity(1,1),
name varchar(50) not null,
marks int not null
)



insert into assignment(name,marks)
values
('Isni', 85),
('Laudry', 75),
('Bambang', 40),
('Anwar', 91),
('Alwi', 70),
('Fulan', 50)

select name, marks, 
case	
	when marks > 90 then 'A+'
	when marks > 70 then 'A'
	when marks > 50 then 'B'
	when marks > 40 then 'C'
else 
	'FAIL'
end [grade] 
from assignment


create table employee
(
id int primary key identity(1,1),
name varchar(50) not null,
salary decimal(18,2) not null
)
