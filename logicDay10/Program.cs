﻿using System;
using System.Collections.Generic;

namespace logicDay10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SimpleArraySum();
        }

        static void SimpleArraySum()
        {
            Console.WriteLine("\t====== Simple Array Sum ======");
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(10);
            list.Add(11);

            int hasil = simpleArraySum(list);
            Console.WriteLine("\t"+hasil);
        }

        static int simpleArraySum(List<int> ar)
        {
            int total = 0;
            for (int i = 0; i < ar.Count; i++)
            {
                total += ar[i];
            }
            return total;

        }
    }
}
