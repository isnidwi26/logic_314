
							----- DDL -----
create database db_kampus2
use db_kampus

--create table
create table mahasiswa (
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null
)

--create view
create view [NamaView]
as --query simpan disini

--alter add column
alter table mahasiswa add [description] varchar(255)

--alter drop column
alter table mahasiswa drop column [description]

--table alter column
alter table mahasiswa alter column email varchar(100) 

--alter view
/*alter view [NameViewOld] to [NameViewNew]*/

--drop database
/*drop database db_kampus2*/

create table mahasiswa2(
id bigint primary key identity(1,1),
name varchar(50) not null,
address varchar(50) not null,
email varchar(255) null)

--drop table 
drop table mahasiswa2

--drop view
drop view [NamaView]

							-----DML-----

--INSERT DATA
insert into mahasiswa(name,address,email) 
values 
--('Alwi','Riau','alwiii@gmail.com'), 
('Toni','Garut','toooniii@gmail.com')

--SELECT DATA
select id, name, address, email from mahasiswa
--atau
select * from mahasiswa
--select custom
select * from mahasiswa where address = 'Garut'

--DELETE DATA
delete from mahasiswa where name = 'Laudry'

--UPDATE DATA
update mahasiswa set address='Sumedang' where name='isni'




					------ LATIHAN VIEW------

--create view
create view vMahasiswa
as 
select id,name,address,email from mahasiswa
--or
select * from vMahasiswa

                  ------- LATIHAN ADD TABLE BIODATA-------
create table biodata(
id bigint primary key identity(1,1),
dob datetime not null,
kota varchar(100) null
)

alter table biodata add mahasiswa_id bigint
alter table biodata alter column mahasiswa_id bigint not null

					

				----JOIN TABLE
select mhs.id as ID, mhs.name,  bio.kota, bio.dob
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
--custom data
where mhs.name = 'toni' or bio.kota = 'bandung'

select mhs.id as ID, mhs.name,  bio.kota, month(bio.dob)Bulan_lahir, year(bio.dob)Tahun_lahir
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id

--panggil data
select * from biodata
select * from mahasiswa


				----ADD DATA
insert into biodata (dob, kota, mahasiswa_id) 
values
('2000/06/26', 'Cimahi','1'),
('1999/09/17', 'Bandung','2'),
('2000/05/30', 'Depok','6'),
('05/08/1998', 'Garut', '8')


--ganti tipe data datetime sebelumnya jadi date aja
alter table biodata alter column dob date


				----ORDER BY
select *
from mahasiswa as mhs
join biodata as bio on mhs.id = bio.mahasiswa_id
order by mhs.id asc, bio.kota desc

				----CONCAT
select CONCAT ('SQL', ' is ', 'fun!.')
select 'SQL' + ' is ' + 'fun!.'

				------ FILTER DATA -------
--1. SELECT TOP
select top 3 * from mahasiswa
order by name asc

--2. BETWEEN
select * from mahasiswa
where id between 1 and 6
--atau
select * from mahasiswa
where id >=4 and id <= 7

--3. LIKE
select * from mahasiswa where name like 'a%' -- huruf awalan a
select * from mahasiswa where name like '%i' -- huruf akhirnya a
select * from mahasiswa where name like '%dh%' -- huruf tengahnya ada huruf tertentu
select * from mahasiswa where name like '_a%' -- huruf kedua nya a
select * from mahasiswa where name like 'a__%' -- huruf depannya a dan minimal hurufnya sebanyak _
select * from mahasiswa where name like 'a_w%'

--4. GROUP BY
select count(id)total_duplikasi , name, address from mahasiswa group by name, address

--5. HAVING
select count(id), name
from mahasiswa
group by name
having count(id) > 1

--6. DISTINCT
select distinct address from mahasiswa

--SUBSTRING
select SUBSTRING ('SQL Tutorial', 1, 3) as judul


				-------- HITUNG LENGTH ---------
--CHAR INDEX
select CHARINDEX('t','Customer') as indeks

--DATA LENGTH
select DATALENGTH ('akumau.istirahat')

					------- CASE WHEN -------
select id, name, address, panjang,
case	
	when panjang < 50 then 'Pendek'
	when panjang <=100 then 'Sedang'
	else 'Tinggi'
end as JarakRumah
from mahasiswa

			----- MENU WARUNG -----
create table penjualan(
id bigint primary key identity(1,1),
nama varchar(50) not null,
harga int not null,
)

insert into penjualan(nama, harga) 
values 
('Indomie', 15000),
('Close-up', 3500),
('Pepsoden', 3000),
('Brush Formula', 2500),
('Roti Manis', 1000),
('Gula', 3500),
('Sarden', 4500),
('Rokok Sampurna', 11000),
('Rokok 234', 11000)

	---operator aritmatika
select *, harga*100 [harga * 100]
from penjualan


