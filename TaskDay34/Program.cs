﻿using System;

namespace TugasDay34
{
    internal class Program
    {
        static void Main(string[] args)
        {
            menu();
            Console.ReadKey();
        }

        static void menu()
        {
            Console.WriteLine("--------------- Selamat datang di kumpulan tugas hari ke 3-4 ---------------");
            Console.WriteLine("\t1. Nilai Mahasiswa");
            Console.WriteLine("\t2. Pulsa");
            Console.WriteLine("\t3. Vocher Grabfood");
            Console.WriteLine("\t4. Vocher Sopi");
            Console.WriteLine("\t5. Tahun Generasi");
            Console.WriteLine("\t6. Gaji Pokok");
            Console.WriteLine("\t7. Menghitung BMI");
            Console.WriteLine("\t8. Nilai Rata-rata");

            Console.Write("\n\tSilahkan pilih menu: ");
            int pilih = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (pilih)
            {
                case 1: tugas1(); break;
                case 2: tugas2(); break;
                case 3: tugas3(); break;
                case 4: tugas4(); break;
                case 5: tugas5(); break;
                case 6: tugas6(); break;
                case 7: tugas7(); break;
                case 8: tugas8(); break;
                default:
                    Console.WriteLine("\tPunten ga ada pilihannya, coba lagi yes!\n");
                    menu();
                    break;

            }
        }

        static void tugas1()
        {
            Console.WriteLine("------------------------------- Tugas 1 --------------------------------\n");
            Console.Write("\tMasukan nilai mahasiswa: ");
            int nilai = int.Parse(Console.ReadLine());

            if (nilai >= 90 && nilai <= 100)
            {
                Console.WriteLine("\tMendapatkan grade A");
            }
            else if (nilai >= 70 && nilai <= 89)
            {
                Console.WriteLine("\tMendapatkan grade B");
            }
            else if (nilai >= 50 && nilai <= 69)
            {
                Console.WriteLine("\tMendapatkan grade C");
            }
            else
            {
                Console.WriteLine("\tMendapatkan grade E");
            }
            Console.WriteLine("\n------------------------------------------------------------------------\n");

        }

        static void tugas2()
        {
            Console.WriteLine("------------------------------- Tugas 2 --------------------------------\n");
            Console.Write("\tMang beli pulsa: ");
            int pulsa = int.Parse(Console.ReadLine());

            if (pulsa >= 100000)
            {
                int poin = 800;
                Console.WriteLine("\tKamu dapat point " + poin + " ya!\n\tDengan pulsa sebanyak Rp. " + pulsa);
            }
            else if (pulsa >= 50000)
            {
                int poin = 400;
                Console.WriteLine("\tKamu dapat point " + poin + " ya!\n\tDengan pulsa sebanyak Rp. " + pulsa);
            }
            else if (pulsa >= 25000)
            {
                int poin = 200;
                Console.WriteLine("\tKamu dapat point " + poin + " ya!\n\tDengan pulsa sebanyak Rp. " + pulsa);
            }
            else if (pulsa >= 10000)
            {
                int poin = 80;
                Console.WriteLine("\tKamu dapat point " + poin + " ya!\n\tDengan pulsa sebanyak Rp. " + pulsa);
            }
            else if (pulsa >= 1)
            {
                int poin = 0;
                Console.WriteLine("\tKamu dapat point " + poin + " ya!\n\tDengan pulsa sebanyak Rp. " + pulsa);
            }
            else
            {
                Console.WriteLine("\tWah ga ada pilihannya, beli yang ada dipilihan aja ya.");
            }
            Console.WriteLine("\n------------------------------------------------------------------------\n");
        }

        static void tugas3()
        {
            Console.WriteLine("------------------------------- Tugas 3 --------------------------------\n");
            Console.WriteLine("\tSilahkan Belanja di Grabfood");
            Console.Write("\tTotal Belanja : ");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("\tJarak : ");
            double jarak = double.Parse(Console.ReadLine());
            Console.Write("\tMasukan kode promo : ");
            string kodePromo = Console.ReadLine().ToLower();

            double diskon, ongkos, totalBelanja;


            if (belanja >= 30000 && kodePromo == "jktovo")
            {

                diskon = 0.4d * belanja;

                if (diskon > 30000)
                {
                    diskon = 30000;
                    if (jarak > 5)
                    {
                        ongkos = 1000 * jarak;
                    }
                    else
                    {
                        ongkos = 5000;
                    }
                }
                else
                {
                    if (jarak > 5)
                    {
                        ongkos = 1000 * jarak;
                    }
                    else
                    {
                        ongkos = 5000;
                    }
                }
            }
            else
            {
                diskon = 0;
                if (jarak > 5)
                {
                    ongkos = 1000 * jarak;
                }
                else
                {
                    ongkos = 5000;

                }

            }
            totalBelanja = belanja - diskon + ongkos;
            Console.WriteLine("\tBelanja : " + belanja);
            Console.WriteLine("\tDiskon (40%) : " + diskon);
            Console.WriteLine("\tOngkir : " + ongkos);
            Console.WriteLine("\tTotal belanja: " + totalBelanja);

            Console.WriteLine("\n------------------------------------------------------------------------\n");
        }

        static void tugas4()
        {
            Console.WriteLine("------------------------------------- Tugas 4 --------------------------------------\n");
        ulang:
            Console.WriteLine("\tSilahkan belanja di Sopi");
            Console.Write("\tMasukan total belanja : ");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("\tOngkos kirim : ");
            int ongkos = int.Parse(Console.ReadLine());
            Console.Write("\tPilih vocher : ");
            int vocher = int.Parse(Console.ReadLine());

            int diskonOngkir = 0, diskonBelanja = 0, totalBelanja;


            if (vocher == 1 && belanja >= 30000)
            {
                diskonOngkir = 5000;
                diskonBelanja = 5000;

            }
            else if (vocher == 2 && belanja >= 50000)
            {
                diskonOngkir = 10000;
                diskonBelanja = 10000;

            }
            else if (vocher == 3 && belanja >= 100000)
            {
                diskonOngkir = 20000;
                diskonBelanja = 10000;

            }
            else
            {
                Console.WriteLine("\n  Vocher tidak tersedia / tidak terdaftar untuk belanjaanmu.\n  Pilihlah vocher yang sesuai");
                Console.Write("  Apakah kamu ingin lanjut menghitung belanjaanmu tanpa vocher?(y/n): ");
                string pilih = Console.ReadLine();
                if (pilih == "n")
                {
                    Console.WriteLine();
                    goto ulang;
                }


            }


            /*else if ((vocher == 1 && belanja < 30000) || (vocher == 2 && belanja < 50000) || (vocher == 3 && belanja < 100000))
            {
                Console.WriteLine("\n  Vocher tidak tersedia / tidak terdaftar untuk belanjaanmu.\n Pilihlah vocher yang sesuai");
                Console.Write("  Apakah kamu ingin lanjut menghitung belanjaanmu tanpa vocher?(y/n): ");
                string pilih = Console.ReadLine();
                if (pilih == "n")
                {
                    Console.WriteLine();
                    goto ulang;
                }


            }
            else
            {
                //vocher selain 1,2,3
                Console.WriteLine("\tVocher tidak terdaftar, silahkan ulangi lagi.");
                Console.WriteLine("\n------------------------------------------------------------------------\n");
                tugas4();
            }*/

            totalBelanja = belanja + ongkos - diskonOngkir - diskonBelanja;
            Console.WriteLine("\n------------------------------------------------------------------------------------\n");
            Console.WriteLine("\tRincian Belanja: ");
            Console.WriteLine("\tBelanja : " + belanja);
            Console.WriteLine("\tOngkos Kirim : " + ongkos);
            Console.WriteLine("\tDiskon Ongkir : " + diskonOngkir);
            Console.WriteLine("\tDiskon Belanja: " + diskonBelanja);
            Console.WriteLine("\tTotal Belanja : " + totalBelanja);
            Console.WriteLine("\n------------------------------------------------------------------------------------\n");

        }

        static void tugas5()
        {
            Console.WriteLine("------------------------------- Tugas 5 --------------------------------\n");
            Console.Write("\tMasukan nama anda\t\t: ");
            string nama = Console.ReadLine();
            Console.Write("\tTahun berapa anda lahir?\t: ");
            int tahun = int.Parse(Console.ReadLine());

            if (tahun >= 1944 & tahun <= 1964)
            {
                Console.WriteLine("\t" + nama + ", berdasarkan tahun lahir anda tergolong Generasi Baby Boomer");
            }
            else if (tahun >= 1965 && tahun <= 1979)
            {
                Console.WriteLine("\t" + nama + ", berdasarkan tahun lahir anda tergolong Generasi X");
            }
            else if (tahun >= 1980 && tahun <= 1994)
            {
                Console.WriteLine("\t" + nama + ", berdasarkan tahun lahir anda tergolong Generasi Y");
            }
            else if (tahun >= 1995 && tahun <= 2015)
            {
                Console.WriteLine("\t" + nama + ", berdasarkan tahun lahir anda tergolong Generasi Z");
            }
            else
            {
                Console.WriteLine("\t" + nama + ", berdasarkan tahun lahir anda tergolong Generasi belum diketahui");
            }
            Console.WriteLine("\n------------------------------------------------------------------------\n");
        }

        static void tugas6()
        {
        ulang:
            Console.WriteLine("------------------------------- Tugas 6 --------------------------------\n");
            Console.Write("\tNama\t\t: ");
            string nama = Console.ReadLine();
            Console.Write("\tTunjangan\t: ");
            int tunjangan = int.Parse(Console.ReadLine());
            Console.Write("\tGapok\t\t: ");
            int gapok = int.Parse(Console.ReadLine());
            Console.Write("\tBanyak Bulan\t: ");
            int banyakBulan = int.Parse(Console.ReadLine());

            int total = gapok + tunjangan;
            double bpjs = Math.Abs((3d / 100d) * total);

            double pajak = 0, gaji, tGaji;


            if ((tunjangan < 0 || gapok < 0) || (tunjangan == 0 && gapok == 0))
            {
                Console.WriteLine("\tHarap masukan data dengan benar.");
                goto ulang;
            }
            else if (total <= 5000000)
            {
                pajak = 0.05d * total;
            }
            else if (total > 5000000 && total <= 10000000)
            {
                pajak = 0.1d * total;
            }
            else if (total > 10000000)
            {
                pajak = 0.15d * total;
            }
            gaji = total - pajak - bpjs;
            tGaji = (total - pajak - bpjs) * banyakBulan;
            Console.WriteLine("\tKaryawan atas nama " + nama + " slip gaji sebagai berikut:");
            Console.WriteLine("\tPajak\t\t\t: Rp." + pajak);
            Console.WriteLine("\tBpjs\t\t\t: Rp." + bpjs);
            Console.WriteLine("\tGaji/Bln\t\t: Rp." + gaji);
            Console.WriteLine("\tTotal gaji/banyak bulan\t: Rp." + tGaji);

            Console.WriteLine("\n------------------------------------------------------------------------\n");
        }

        static void tugas7()
        {
            Console.WriteLine("------------------------------- Tugas 5 --------------------------------\n");
            Console.Write("\tMasukan berat badan anda (kg)\t: ");
            double berat = double.Parse(Console.ReadLine());
            Console.Write("\tMasukan tinggi badan anda (cm)\t: ");
            double tinggi = double.Parse(Console.ReadLine());

            double bmi = berat / Math.Pow((tinggi / 100d), 2);

            if (bmi < 18.5d)
            {
                Console.WriteLine("\tNilai BMI anda adalah\t\t: " + Math.Round(bmi, 4));
                Console.WriteLine("\tAnda termasuk berbadan kurus");
            }
            else if (bmi >= 18.5d && bmi < 25)
            {
                Console.WriteLine("\tNilai BMI anda adalah\t\t: " + Math.Round(bmi, 4));
                Console.WriteLine("\tAnda termasuk langsing");
            }
            else
            {
                Console.WriteLine("\tNilai BMI anda adalah\t\t: " + Math.Round(bmi, 4));
                Console.WriteLine("\tAnda termasuk gemuk");
            }
            Console.WriteLine(new string('-', 72));
        }

        static void tugas8()
        {
            Console.WriteLine("------------------------------- Tugas 8 --------------------------------\n");
            Console.Write("\tMasukan Nilai MTK\t: ");
            int mtk = int.Parse(Console.ReadLine());
            Console.Write("\tMasukan nilai Fisika\t: ");
            int fisika = int.Parse(Console.ReadLine());
            Console.Write("\tMasukan Nilai Kimia\t: ");
            int kimia = int.Parse(Console.ReadLine());

            int rataRata = (mtk + fisika + kimia) / 3;
            if (rataRata <= 50)
            {
                Console.WriteLine("\t---------------------------------");
                Console.WriteLine("\tNilai Rata - Rata\t: " + rataRata);
                Console.WriteLine("\tMaaf\n\tKamu Gagal");
            }
            else
            {
                Console.WriteLine("\t---------------------------------");
                Console.WriteLine("\tNilai Rata - Rata\t: " + rataRata);
                Console.WriteLine("\tSelamat\n\tKamu Berhasil\n\tKamu Hebat");
            }
            Console.WriteLine("\n------------------------------------------------------------------------\n");
        }

    }
}
