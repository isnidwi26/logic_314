﻿using System;

namespace logicDay01 //suatu nama yang menampung banyak class
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //perbedaannya adalah jika write saja tidak akan memberikan enter space
            //jika WriteLine memberikan enter space
            /*
            Console.WriteLine("Assalamu'alaikum!");
            Console.WriteLine("Hai semua");
            Console.Write("Haiiiii");
            Console.Write("Hai juga");
            Console.WriteLine("Haloo");


            Console.Write("Perkenalkan namaku ");
            String nama = Console.ReadLine();
            Console.WriteLine($"Oooohh namamu {nama}");
            Console.WriteLine("Oooohh kamu {0}", nama);
            Console.WriteLine("Oooohh nama kamu " + nama);*/




            Konversi();
            OperatorAritmatika();
            Modulus();
            OperatorPenugasan();
            operatorPerbandingan();
            operatorLogika();
            penjumlahan();



            Console.ReadKey();
        }



        static void Konversi()
        {
            Console.WriteLine("~~~~~Konversi~~~~~");
            int umur = 23;
            String strUmur = umur.ToString();
            Console.WriteLine(strUmur);

            int myInt = 12;
            double myDouble = 22.5;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConvDouble = Convert.ToDouble(myInt);
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to String : " + myString);
            Console.WriteLine("Convert int to double : " + myConvDouble);
            Console.WriteLine("Convert double to Int : " + myConvInt);
            Console.WriteLine("Convert bool to string : " + myConvString);
        }

        static void OperatorAritmatika()
        {

            int alpukat, anggur, total = 0;

            Console.WriteLine(" \n~~~~~Operator Aritmatika~~~~~");
            Console.Write(" Jumlah alpukatmu adalah : ");
            alpukat = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Anggurmu adalah : ");
            anggur = Convert.ToInt32(Console.ReadLine());

            total = alpukat + anggur;

            Console.WriteLine($"Total buah-buahan yang kamu miliki adalah : {total} \n");
        }

        static void Modulus()
        {
            Console.WriteLine("~~~~~Perhitungan Modulus~~~~~");
            int jeruk, kawan, hasil = 0;
            Console.Write("Jumlah buah jeruk yang ada sebanyak : ");
            jeruk = int.Parse(Console.ReadLine());
            Console.Write("Jumlah teman-teman sebanyak : ");
            kawan = int.Parse(Console.ReadLine());

            hasil = jeruk % kawan;

            Console.WriteLine($"Hasil jeruk % kawan = {hasil}\n");
        }

        static void OperatorPenugasan()
        {
            int mangga = 12;
            int apel = 4;

            mangga = 13;
            apel += 7;

            Console.WriteLine("~~~~~Operator Penugasan~~~~~");

            Console.WriteLine("mangga: " + mangga);
            Console.WriteLine("Apel: " + apel);

            apel -= mangga;
            Console.WriteLine("Buah sekarang bersisa : " + apel);
            apel += mangga;
            Console.WriteLine("Buah sekarang bersisa : " + apel);
            mangga *= apel;
            Console.WriteLine("Buah sekarang tersisa : " + mangga);
            mangga /= apel;
            Console.WriteLine("Buah sekarang tersisa : " + mangga);
            mangga %= apel;
            Console.WriteLine("Buah sekarang tersisa : " + mangga);

        }

        static void operatorPerbandingan()
        {
            Console.WriteLine("~~~~~ Operator Perbandingan ~~~~~");

            int jeruk, apel = 0;

            Console.Write("Jeruk yang ada sebanyak = ");
            jeruk = int.Parse(Console.ReadLine());
            Console.Write("Apel yang ada sebanyak = ");
            apel = int.Parse(Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine("Hasil Perbandingan dari kedua jenis buah tersebut adalah: ");
            Console.WriteLine("jeruk > apel : " + (jeruk > apel));
            Console.WriteLine("jeruk >= apel : " + (jeruk >= apel));
            Console.WriteLine("jeruk < apel : " + (jeruk < apel));
            Console.WriteLine("jeruk <= apel : " + (jeruk <= apel));
            Console.WriteLine("jeruk == apel : " + (jeruk == apel));
            Console.WriteLine("jeruk != apel : " + (jeruk != apel));

        }

        static void operatorLogika()
        {
            Console.WriteLine("\n~~~~~ Operator Logika ~~~~~");

            Console.Write("Masukan umur anda: ");
            int age = int.Parse(Console.ReadLine());
            Console.Write("Masukan kata sandi: ");
            string pass = Console.ReadLine();

            bool isAdult = age > 18;
            bool isPassValid = pass == "admin";

            if (isAdult && isPassValid)
            {
                Console.WriteLine("Selamat datang kawan!.");
            }
            else if (isAdult && !isPassValid)
            {
                Console.WriteLine("Maaf kawan, kata sandimu salah.");
            }
            else if (!isAdult && isPassValid)
            {
                Console.WriteLine("Maaf kawan, umurmu kurang.");
            }
            else
            {
                Console.WriteLine("Maaf kawan, kamu belum cukup umur dan kata sandimu salah. Harap coba lagi ya!");
            }
        }

        static int operatorHitung(int buah1, int buah2)
        {
            int total = 0;
            total = buah1 + buah2;
            return total;
        }

        static void penjumlahan()
        {
            int alpukat, anggur;

            Console.WriteLine(" \n~~~~~Operator Aritmatika~~~~~");
            Console.Write("Jumlah alpukatmu adalah : ");
            alpukat = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Anggurmu adalah : ");
            anggur = Convert.ToInt32(Console.ReadLine());

            int total = operatorHitung(alpukat, anggur);
            Console.WriteLine("Hasil alpukat + anggur kamu sebanyak : " + total);
        }


    }
}
