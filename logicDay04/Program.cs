﻿using System;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;
using System.Threading;

namespace logicDay04
{
    internal class Program
    {
        static void Main(string[] args)
        {
            menu();

            Console.ReadKey();
        }

        static void menu()
        {
            Console.WriteLine("--------------- Selamat datang di hari ke-4 ---------------");
            Console.WriteLine("\t1. Remove String");
            Console.WriteLine("\t2. Insert String");
            Console.WriteLine("\t3. Replace String");
        


            Console.Write("\n\tSilahkan pilih menu: ");
            int pilih = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (pilih)
            {
                case 1: removeString(); break;
                case 2: insertString(); break;
                case 3: replaceString(); break;            
                default:
                    Console.WriteLine("\tPunten ga ada pilihannya, coba lagi yes!\n");
                    menu();
                    break;

            }
        }

        static void removeString()
        {
            Console.WriteLine("***** Remove String *****");
            Console.Write("Masukan kalimat: ");
            string kal = Console.ReadLine();
            Console.Write("Isi parameter Remove: ");
            int indeks = int.Parse(Console.ReadLine());

            Console.WriteLine("Hasil Remove String: "+kal.Remove(indeks));
        }

        static void insertString()
        {
            Console.WriteLine("***** Insert String *****");
            Console.Write("Masukan kalimat: ");
            string kal = Console.ReadLine();
            Console.Write("Isi parameter Insert: ");
            int indeks = int.Parse(Console.ReadLine());
            Console.Write("Masukkan input string: ");
            string input = Console.ReadLine();

            Console.WriteLine("Hasil Insert String: " + kal.Insert(indeks,input));
        }

        static void replaceString()
        {
            Console.WriteLine("****** Replace String ******");
            Console.Write("Masukan kalimat: ");
            string kal = Console.ReadLine();
            Console.Write("Kata awal: ");
            string kataLama = Console.ReadLine();
            Console.Write("Kata yang akan diganti: ");
            string kataBaru = Console.ReadLine();

            Console.WriteLine("Hasil Replace: " + kal.Replace(kataLama,kataBaru));

        }

    }
}
