﻿using System;
using System.Collections.Generic;
using System.Text;

namespace logicDay07
{
    public abstract class CalculatorAbstract
    {
        public abstract int jumlah(int x, int y);
        public abstract int kurang(int x, int y);
    }
    public class testTurunan : CalculatorAbstract
    {
        public override int jumlah(int x, int y)
        {
            return x + y;
        }
        public override int kurang(int x, int y)
        {
            return x - y;
        }
       
    }
}
