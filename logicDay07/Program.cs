﻿using System;
using System.Collections.Generic;

namespace logicDay07
{ 
    public class Program
    {
        static List<User> users = new List<User>();
        static void Main(string[] args)
        {
            //insertUser();
            //dateTime();
            //stringDT();
            timespan();
            //panggilClassInheritance();
            //overriding();


        }

        static void insertUser()
        {

            /*List<User> users = new List<User>()
            {
                new User(){nama ="Firdha", umur = 24, alamat= "Tangerang Selatan"},
                new User(){nama = "Isni", umur = 22, alamat = "Cimahi"},
                new User(){nama = "Asti", umur = 23, alamat = "Garut"},
                new User(){nama = "Muafa", umur = 22, alamat = "Bogor"},
                new User(){nama = "Toni", umur = 24, alamat = "Garut"}
            };

            Console.WriteLine("\t============================ LIST ANGGOTA BOOTCAMP ============================");
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine("\t" + users[i].jenisKelamin + " bernama " + users[i].nama + " berasal dari " + users[i].alamat + " dengan usia " + users[i].umur + " tahun");
            }
            Console.WriteLine("\t===============================================================================");*/

            ulang:
            Console.Write("\tMasukkan nama anggota: ");
            string nama = Console.ReadLine();
            Console.Write("\tMasukkan umur: ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("\tMasukkan alamat: ");
            string alamat = Console.ReadLine();
            Console.Write("\tMasukkan jenis kelamin: ");
            string jenisKelamin = Console.ReadLine();

            //nambah data list
            User user = new User();
            user.nama = nama;
            user.umur = umur;
            user.alamat = alamat;
            user.jenisKelamin = jenisKelamin;
            users.Add(user);

            Console.WriteLine("\t============================ LIST ANGGOTA BOOTCAMP BARU ============================");
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine("\t"+ users[i].jenisKelamin + " bernama " + users[i].nama + " berasal dari " + users[i].alamat + " dengan usia " + users[i].umur + " tahun");
            }
            Console.WriteLine("\t====================================================================================");

            Console.Write("\tApakah ingin menambahkan anggota?(y/n): ");
            char pilih = char.Parse(Console.ReadLine().ToLower());
            if (pilih == 'y')
            {
                Console.Clear();
                goto ulang;
            }
            
        }

        static void dateTime()
        {
            DateTime firstDT = new DateTime();
            Console.WriteLine(firstDT);

            DateTime dtNow = DateTime.Now;
            Console.WriteLine(dtNow);

            DateTime date = DateTime.Now.Date;
            Console.WriteLine(date);

            DateTime zonadt = DateTime.UtcNow;
            Console.WriteLine(zonadt);

            DateTime inputTanggal = new DateTime(2023, 03, 09);
            Console.WriteLine(inputTanggal);

            DateTime inputDateTime = new DateTime(2023, 3,9, 10, 43, 00);
            Console.WriteLine(inputDateTime);



        }

        static void stringDT()
        {
            Console.WriteLine("\t+++++++++++++++++++ String Date Time +++++++++++++++++++");
            Console.Write("\tMasukkan tanggal (dd/mm/yy): ");
            string tanggal = Console.ReadLine();

            DateTime strTanggal = DateTime.Parse(tanggal);
            Console.WriteLine("\tTanggal: " + strTanggal.Day);
            Console.WriteLine("\tBulan: " + strTanggal.Month);
            Console.WriteLine("\tTahun: " + strTanggal.Year);
            Console.WriteLine("\tHari ke-" + (int)strTanggal.DayOfWeek);
            Console.WriteLine("\tHari : " + strTanggal.DayOfWeek);
            Console.WriteLine("\tHari ke-" + strTanggal.DayOfYear);
            Console.WriteLine("\t++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        }
        
        static void timespan()
        {
            Console.WriteLine("\t+++++++++++++++++++ TIMESPAN +++++++++++++++++++");
            DateTime date1 = new DateTime(2023, 1, 1, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            TimeSpan span = date2.Subtract(date1);
            //TimeSpan span = date2 - date1;

            Console.WriteLine("\tTotal hari: " + span.Days);
            Console.WriteLine("\tTotal hari: " + span.TotalDays);
            Console.WriteLine("\tTotal beda jam: " + span.Hours);
            Console.WriteLine("\tTotal jam: " + span.TotalHours);
            Console.WriteLine("\tTotal beda menit: " + span.Minutes);
            Console.WriteLine("\tTotal menit: " + span.TotalMinutes);
            Console.WriteLine("\tTotal beda detik: " + span.Seconds);
            Console.WriteLine("\tTotal detik: " + span.TotalSeconds);



            Console.WriteLine("\t++++++++++++++++++++++++++++++++++++++++++++++++++");



        }

        static void panggilClassInheritance()
        {
            TypeMobil mobil = new TypeMobil();
            mobil.kecepatan = 120;
            mobil.posisi = 50;
            mobil.Civic();
        }

        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();

        }
    }

    //CONTOH OVERRIDING
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Lari.....");
        }
    }

    class Kucing : Mamalia { }


    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang....");
        }
    }



}
