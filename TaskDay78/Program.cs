﻿using System;
using System.ComponentModel.Design;
using System.Globalization;
using System.Linq;
using System.Net.Sockets;

namespace TaskDay78
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //faktorial();
            //sos();
            //hitungHurufdanKonsonan();
            //namaBerbintang();
            //segitigaBerbintang();
            //pengurutan();
            //patunganMakan();
            //lilin();
            //matriks();
            rotasi();
            //pinjamBuku();
            //waktuUjian();

            


        }
        static void waktuUjian()
        {
            Console.WriteLine("========================= Waktu Ujian =========================");
            Console.Write("Tanggal pemberitahuan: ");
            DateTime tanggalPengumuman = DateTime.Parse(Console.ReadLine());

            Console.Write("Masa tunggu ujian: ");
            int masaTunggu = int.Parse(Console.ReadLine());

            Console.Write("Hari libur: ");
            int[] hariLibur = Array.ConvertAll( Console.ReadLine().Split(','), int.Parse);

            DateTime tanggalUjian = tanggalPengumuman;
                        

            for(int i = 0; i<masaTunggu; i++)
            {
                if (i>0) 
                    tanggalUjian = tanggalUjian.AddDays(1);

                if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
                    tanggalUjian = tanggalUjian.AddDays(2);

                for(int j=0; j<hariLibur.Length; j++)
                {
                    if (tanggalUjian.Day == hariLibur[j])
                    {
                        tanggalUjian = tanggalUjian.AddDays(1);

                        if(tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
                            tanggalUjian = tanggalUjian.AddDays(2);
                    }

                }
            }

            tanggalUjian = tanggalUjian.AddDays(1);
            if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
                tanggalUjian = tanggalUjian.AddDays(2);

            Console.WriteLine("Ujian kelas akan dimulai tanggal: " + tanggalUjian.ToString("dddd, dd/MMMM/yyyy"));

            Console.WriteLine("===============================================================");
        }

        static void rotasi()
        {
            Console.WriteLine("\t=============== Rotasi Angka ===============");
            Console.Write("\tMasukkan data angka dengan pemisah (spasi): ");
            int[] angka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
            Console.Write("\tJumlah rotasi: ");
            int rotasi = int.Parse(Console.ReadLine());
            int rot=0;

            for(int i = 0; i < rotasi; i++)
            {
                for(int j = 0; j < angka.Length; j++)
                {
                    if (j < 1)
                    {
                        rot = angka[j];
                        angka[j] = angka[j+1];                       
                    }else if(j >= 1 && j < angka.Length - 1)
                        angka[j] = angka[j + 1];
                    else
                        angka[angka.Length - 1] = rot;                    
                }
            }
            Console.Write("\t");
            /*
            for(int i = 0;i < angka.Length; i++)
            {
                Console.Write(angka[i]+ " ");
            }*/
            Console.Write(string.Join(" ",angka));
        }

        static void pinjamBuku()
        {
            Console.WriteLine("\t=============== Pinjam Buku ===============");
            Console.Write("Masukkan tanggal pinjam (dd/mm/yy): ");
            DateTime pinjam = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukkan tanggal pengembalian (dd/mm/yy): ");
            DateTime kembali = DateTime.Parse(Console.ReadLine());

            TimeSpan total = kembali.Subtract(pinjam);

            int denda = 500, bayarDenda;
            if(total.Days > 3)
            {
                bayarDenda = (total.Days - 3) * denda;

                Console.WriteLine("Kamu harus bayar denda sebesar: " + bayarDenda);
            }
            else
            {
                Console.WriteLine("Terimakasih sudah meminjam buku.");
            }

            Console.WriteLine("\t===========================================");
        }

        static void matriks()
        {
            //PERBAIKAN
            Console.WriteLine("\t=============== Matriks ===============");
            int[,] angka = new int[3,3];
            for(int i = 0; i < angka.GetLength(0); i++)
            {
                for (int j =0; j < angka.GetLength(1); j++)
                {
                    Console.Write("\tMasukkan angka matriks [" + i +","+ j+ "]: ");
                    angka[i, j] = int.Parse(Console.ReadLine());
                }
            }
            for(int i = 0; i<angka.GetLength(0); i++) 
            { 
                Console.Write("\t\t");
                for(int j = 0; j < angka.GetLength(1); j++)
                {
                    Console.Write(angka[i, j] + ",");
                }
                Console.WriteLine();
            }

            int diagonal1=0, diagonal2=0;
            string tampungdiagonal1="", tampungdiagonal2="";
            //int diagonal1 = angka[0,0] + angka[1,1] + angka[2,2];
            //int diagonal2 = angka[0,2] + angka[1,1] + angka[2,0];

            for(int i =0; i<angka.GetLength(0); i++)
            {
                diagonal1 += angka[i, i];
                tampungdiagonal1 += tampungdiagonal1 == "" ? angka[i, i].ToString() : angka[i,i]<0 ? " - " + Math.Abs(angka[i,i]).ToString() : " + " + angka[i, i].ToString();


            }

            for (int i = 0; i < angka.GetLength(0); i++)
            {
                diagonal2 += angka[i, angka.GetLength(0) - 1 - i];
                tampungdiagonal2 += tampungdiagonal2 == "" ? angka[i, angka.GetLength(0) - 1 - i].ToString() : " + " + angka[i, angka.GetLength(0) - 1 - i].ToString();

            }

            int totalDiagonal = diagonal1 - diagonal2;

            Console.WriteLine($"\tDiagonal 1 : {tampungdiagonal1} : {diagonal1}");
            Console.WriteLine($"\tDiagonal 1 : {tampungdiagonal2} : {diagonal2}");
            Console.WriteLine($"\tPerbedaan |{diagonal1}-{diagonal2}|={totalDiagonal}");
            Console.WriteLine("\t=======================================");
        }

        static void lilin()
        {
            Console.WriteLine("\t========= Menghitung lilip yang tertiup =========");
            Console.Write("\tMasukkan panjang lilin: ");
            int[] lilin = Array.ConvertAll(Console.ReadLine().Split(" "),int.Parse);

            int maks = 0, banyakmaks=0;

            /*for(int i = 0; i < lilin.Length; i++)
            {
                for(int j = 0; j< lilin.Length; j++)
                {
                    if (lilin[i] <= lilin[j])
                    {
                        maks = lilin[i];
                        lilin[i] = lilin[j];
                        lilin[j] = maks;
                    }
                }
            }*/

            for (int j = 0; j < lilin.Length; j++)
            {
                if (lilin[j] > maks)
                    maks = lilin[j];
                if (lilin[j] == maks)
                    banyakmaks++;
            }

            Console.WriteLine("\tNilai lilin tertinggi adalah: " + maks);
            Console.WriteLine("\tJumlah lilin tertinggi sebanyak: " + banyakmaks);
            Console.WriteLine("\t=================================================");

        } 

        static void patunganMakan()
        {
            Console.WriteLine("\t====================== Patungan Makan ======================");
            Console.Write("\tTotal Menu yang diambil: ");
            int banyakMenu = int.Parse(Console.ReadLine());
            Console.Write("\tMakanan yang alergi ada di ke-");
            int menuAlergi = int.Parse(Console.ReadLine());

            int[] hargaMenu = new int[banyakMenu];
            int totalHarga = 0;


            for(int i = 0; i < banyakMenu; i++)
            {
                Console.Write("\tHarga menu ke-" + (i+1) + ": " );
                hargaMenu[i] = int.Parse(Console.ReadLine());
                totalHarga += hargaMenu[i];
            }

            Console.Write("\tUang yang dimiliki Elsa sebesar: ");
            int uangElsa = int.Parse(Console.ReadLine());
            
            totalHarga -= hargaMenu[menuAlergi];
            totalHarga /= 2;

            Console.WriteLine("\tUang patungan yang harus Elsa bayar sebesar: " + totalHarga.ToString("Rp #,##0.00"));
            int sisaUang = uangElsa - totalHarga;
            if( sisaUang == 0 ) 
            {
                Console.WriteLine("\tTidak ada sisa, uangnya pas");    
            }
            else if(sisaUang < 0)
            {
                Console.WriteLine("\tUangnya kurang " + Math.Abs(sisaUang).ToString("Rp -#,##0.00"));
            }
            else
                Console.WriteLine("\tSisa uang Elsa : " + sisaUang.ToString("Rp #,##0.00"));
            Console.WriteLine("\t============================================================");
        }

        static void pengurutan()
        {
            Console.WriteLine("\t=========== Pengurutan ===========");
            Console.Write("\tMasukan jumlah angka: ");
            int range = int.Parse(Console.ReadLine());
            int[] angka = new int[range];

            for(int i = 0; i < range; i++)
            {
                Console.Write("\tMasukkan angka ke-" + (i+1) + ": " );
                angka[i] = int.Parse(Console.ReadLine());
            }
            int min = 0;
            for (int i = 0; i < range; i++)
            {
                for (int j = 0; j < range; j++)
                {
                    if (angka[i] < angka[j])
                    {
                        min = angka[i];
                        angka[i] = angka[j];
                        angka[j] = min;
                    }
                }
            }
            Console.WriteLine("\tHasil pengurutan: ");
            for (int i = 0; i < range; i++)
            {
                Console.Write("\t" + angka[i] + " ");
            }

            Console.WriteLine("\t==================================");

        }

        static void segitigaBerbintang()
        {
            Console.WriteLine("\t=========== Segitiga Berbintang ===========");
            Console.Write("\tMasukkan panjang segitiga: ");
            int panjang = int.Parse(Console.ReadLine());

            /*for(int i = 1; i <= panjang; i++)
            {
                Console.Write("\t\t\t");
                for (int j = panjang - 1; j >= i; j--)
                {
                    Console.Write(" ");
                }
                for (int k = 1; k <= i; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }*/

            //CARA BAPAK
            for(int i = 0; i < panjang; i++)
            {
                Console.Write("\t\t\t");
                for (int j = 0  ; j < panjang ; j++)
                {
                    if (j < panjang - 1- i)
                        Console.Write(" ");
                    else
                        Console.Write("*");
                }
                Console.WriteLine();
            }
        }

        static void namaBerbintang()
        {
            Console.WriteLine("\t====== Nama berbintang ======");
            Console.Write("\tMasukkan nama anda: ");
            //string[] nama = Console.ReadLine().ToLower().Split(" ");
            string nama = Console.ReadLine().Replace(" ","");
            string bintang = "***";

            foreach (char huruf in  nama)
            {
                Console.WriteLine("\t" + bintang + huruf + bintang);

            }
           /* for(int i = 0; i < nama.Length; i++)
            {
                for(int j = 0; j < nama[i].Length; j++)
                {
                    Console.WriteLine("\t" + bintang + nama[i][j] + bintang);
                }
            }*/


            /*for(int i = 0; i < 7; i++)
            {
                if (i == 3)
                {
                    for (int j = 0; j < nama.Length; j++)
                    {
                        for(int k=0; k < nama[j].Length; k++)
                        {
                            Console.Write("\t" + nama[j][k]);
                        }
                    }
                }
                else
                {
                    Console.Write('*');
                }
            }*/

        }

        static void hitungHurufdanKonsonan()
        {
            Console.WriteLine("\t====== Menghitung huruf vokal dan huruf konsonan ======");
            Console.Write("\tMasukkan kalimat: ");
            //string[] huruf = Console.ReadLine().Split(" ");
            string kalimat =  Console.ReadLine().ToLower();
            int totalVokal = 0, totalKonsonal = 0;
            /*for (int i = 0; i < huruf.Length; i++)
            {
                for(int j = 0; j < huruf[i].Length; j++)
                {
                    if (huruf[i][j] == 'a' || huruf[i][j] == 'i' || huruf[i][j] == 'u' || huruf[i][j] == 'e' || huruf[i][j] == 'o')
                        totalVokal++;
                    else
                        totalKonsonal++;
                }
            }*/

            //CARA FIRDHA

            foreach (char huruf in kalimat)
            {
                if(huruf >= 'a' && huruf <= 'z')
                {
                    if (huruf == 'a' || huruf == 'i' || huruf == 'u' || huruf == 'e' || huruf == 'o')
                        totalVokal++;
                    else
                        totalKonsonal++;
                }
            }
            Console.WriteLine("\tJumlah huruf vokal sebanyak " +  totalVokal);
            Console.WriteLine("\tJumlah huruf konsonan sebnayak " + totalKonsonal);
            Console.WriteLine("\t=======================================================");
        }

        static void faktorial()
        {
            Console.WriteLine("\t\t=============== Faktorial ===============");
            Console.Write("\t\tMasukkan x: ");
            int x = int.Parse(Console.ReadLine());
            int y = 1;
            string nampung="";
            Console.Write("\t\t");
            for (int i = x; i >= 1; i--)
            {
                y *= i;
                //CARA ISNI
                /*if (i>1)
                {
                    nampung = i + " x ";
                }
                else
                {
                    nampung = i + " = ";
                }
                 Console.Write(nampung);*/

                //CARA BAPAK
                nampung += nampung == "" ? i.ToString() : "*" + i.ToString();
            }
            Console.WriteLine (x + "! =" + nampung + " = " + y);
            Console.WriteLine("\t\tAda " + y + " cara."  );
        }

        static void sos()
        {
            Console.WriteLine("\t=============== Cek SOS ===============");
            Console.Write("\tMasukan kode : ");
            char[] kode = Console.ReadLine().ToUpper().ToCharArray();
            int ngitung=0,ngitungSalah=0;
            string yangBenar = "";
            if(kode.Length %3 == 0)
            {
                for(int i = 0; i < kode.Length; i+=3)
                {
                    /*if (morse[i] == 'S' && morse[i+1] == 'O' && morse[i+2] == 'S')
                    {
                        ngitung++;
                    }
                    else 
                    {
                        ngitungSalah++;
                    }*/

                    if (kode[i] != 'S' || kode[i + 1] != 'O' || kode[i + 2] != 'S')
                    {
                        ngitungSalah++;
                        yangBenar += "SOS";

                    }
                    else
                    {
                        yangBenar += kode[i].ToString() + kode[i + 1].ToString() + kode[i + 2].ToString();
                        ngitung++;
                    }
                
                }
            }
            else
            {
                Console.WriteLine("\tKode sinyal kurang!");
            }

            Console.WriteLine("\tJumlah SOS yang benar sebanyak " + ngitung);
            Console.WriteLine("\tJumlah SOS yang salah sebanyak " + ngitungSalah);
            Console.WriteLine("\tSinyal yang diterima " + string.Join("", kode));
            Console.WriteLine("\tSinyal yang benar adalah " +  yangBenar);
        }


    }
}
