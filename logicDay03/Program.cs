﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Schema;

namespace logicDay03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            pilihMenu();
            Console.ReadKey();
        }
        
        static void pilihMenu()
        {
            Console.WriteLine("---------------- Selamat datang di hari ke-3 ----------------");
            Console.WriteLine("\t1. Latihan soal pertama");
            Console.WriteLine("\t2. Latihan soal kedua");
            Console.WriteLine("\t3. Latihan soal kettiga");
            Console.WriteLine("\t4. Latihan soal keempat");
            Console.WriteLine("\t5. Perulangan 'While'");
            Console.WriteLine("\t6. Perulangan 'Do'");
            Console.WriteLine("\t7. Perulangan 'For'");
            Console.WriteLine("\t8. Perulangan bersarang");
            Console.WriteLine("\t9. Perulangan ForEach");
            Console.WriteLine("\t10. Array");
            Console.WriteLine("\t11. Array 2 Dimensi");
            Console.WriteLine("\t12. Split dan Join");
            Console.WriteLine("\t13. Substring");
            Console.WriteLine("\t14. Convert to char array");

            Console.Write("\n\tSilahkan pilih menu: ");
            int pilih = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (pilih)
            {
                case 1:latihanSoal1(); break;
                case 2: latihanSoal2(); break;
                case 3: latihanSoal3(); break;
                case 4: latihanSoal4(); break;
                case 5: loopingWhile(); break;
                case 6: loopingDo(); break;
                case 7: loopingFor(); break;
                case 8: perulanganBersarang(); break;
                case 9: perulanganForEach(); break;
                case 10: array();break;
                case 11: array2Dimensi();break;
                case 12: splitJoin(); break;
                case 13: subString();break;
                case 14: convertToCharArray(); break;
                default:
                    Console.WriteLine("\tPunten ga ada pilihannya, coba lagi yes!\n");
                    pilihMenu();
                    break;

            }


        }

        static void latihanSoal1()
        {
            Console.WriteLine("----------------------- Latihan Soal 1 --------------------");
            int nilai;
            Console.Write("\tMasukan nilaimu: ");
            nilai = int.Parse(Console.ReadLine());


            if (nilai >= 80 && nilai <= 100)
            {
                Console.WriteLine("\tNilaimu Bagus, kamu mendapatkan A.");
            }
            else if (nilai <= 79 && nilai >= 60)
            {
                Console.WriteLine("\tNilaimu cukup, kamu mendapatkan B.");
            }
            else if (nilai < 0 && nilai > 100)
            {
                Console.WriteLine("\tNilai yang kamu inputkan salah.");
            }
            else
            {
                Console.WriteLine("\tNilaimu kurang, kamu mendapatkan C.");
            }
            Console.WriteLine("-----------------------------------------------------------\n");

        }

        static void latihanSoal2()
        {
            Console.WriteLine("----------------------- Latihan Soal 2 --------------------");
            Console.Write("\tMasukan angka: ");
            int angka = int.Parse(Console.ReadLine());

            if (angka % 2 == 0)
            {
                Console.WriteLine("\tAngka " + angka + " adalah bilangan genap");
            }
            else
            {
                Console.WriteLine("\tAngka " + angka + " adalah bilangan ganjil");
            }
            Console.WriteLine("-----------------------------------------------------------\n");
        }

        static void latihanSoal3()
        {
            Console.WriteLine("----------------------- Latihan Soal 3 --------------------");
            Console.Write("\tMasukan angka: ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("\tMasukan angka pembagi: ");
            int pembagi = int.Parse(Console.ReadLine());

            if (angka % pembagi == 0)
            {
                Console.WriteLine("\tAngka " + angka + " % " + pembagi + " adalah 0");
            }
            else
            {
                Console.WriteLine("\tAngka " + angka + " % " + pembagi + " bukan 0 melainkan hasil mod " +(angka%pembagi));
            }

            Console.WriteLine("-----------------------------------------------------------\n");
        }

        static void latihanSoal4()
        {
            Console.WriteLine("----------------------- Latihan Soal 4 --------------------");
            Console.Write("\tMasukan jumlah puntung rokok yang kamu punya: ");
            int pRokok = int.Parse(Console.ReadLine());
            int hJual = 500;
            int penghasilan = hJual * (pRokok / 8);

            if (pRokok % 8 == 0)
            {
                
                Console.WriteLine("\tKamu mendapatkan penghasilan sebanyak : " + penghasilan);
            }
            else
            {

                int sisa = pRokok % 8;
                Console.WriteLine("\tKamu mendapatkan penghasilan sebanyak : " + penghasilan +
                    "\n\tdan kamu masih memiliki sisa puntung rokok " + sisa);

            }

            Console.WriteLine("-----------------------------------------------------------\n");
        }

        static void loopingWhile()
        {
            Console.WriteLine("----------------------- Perulangan 'While' --------------------");
            bool loop = true;
            int nilai = 1;

            while(loop)
            {
                Console.WriteLine("  Proses ke-"+nilai);
                

                Console.Write("\tApakah anda ingin mengulangi proses ini (y/n)? ");
                string proses = Console.ReadLine().ToLower();
                if (proses == "y")
                {
                    loop = true;
                }
                else if(proses == "n")
                {
                    loop = false;
                }
                else
                {
                    Console.WriteLine("Yang anda inputkan salah silahkan ulangi lagi.");
                    loop &= false;
                    nilai = 1;
                }
                nilai++;
            }

            Console.WriteLine("-----------------------------------------------------------\n");
        }

        static void loopingDo()
        {
            Console.WriteLine("----------------------- Perulangan 'Do-While' --------------------");
            int a = 0;
            do
            {
                Console.WriteLine("Proses ke-" + a);
                a++;
            } while (a < 5);
            Console.WriteLine("------------------------------------------------------------------\n");

        }

        static void loopingFor()
        {
            Console.Write("Masukan jumlah (n) yang kamu inginkan: ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan posisi indeks yang akan di break atau di continue: ");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("----------------------- Perulangan 'For Increment' --------------------\n");
            for(int i=0; i < n; i++)
            {
                Console.WriteLine("\t\t\t\t" + (i+1));
            }

            Console.WriteLine("\n-----------------------------------------------------------------------\n");

            Console.WriteLine("----------------------- Perulangan 'For Decrement' --------------------\n");
            for (int i = n; i > 0; i--)
            {
                Console.WriteLine("\t\t\t\t" + (i));
            }

            Console.WriteLine("\n-----------------------------------------------------------------------\n");

            Console.WriteLine("----------------------- Perulangan 'For - Break' ----------------------\n");
            for (int i = 0; i < n; i++)
            {
                if (i == m)
                {
                    break;
                }
                Console.WriteLine("\t\t\t\t" + (i+1));
            }

            Console.WriteLine("\n-----------------------------------------------------------------------\n");



            Console.WriteLine("----------------------- Perulangan 'For - Continue' ----------------------\n");
            for (int i = 0; i < n; i++)
            {
                if (i == m)
                {
                    continue;
                }
                Console.WriteLine("\t\t\t\t" + (i + 1));
            }

            Console.WriteLine("\n------------------------------------------------------------------------\n");

        }

        static void perulanganBersarang()
        {
            Console.WriteLine("----------------------- Perulangan Bersarang ----------------------\n");
            
            for (int i=0; i<3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write("\t\t|"+ i +","+ j+"|");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n-------------------------------------------------------------------\n");

            int[,] array = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            for(int i = 0; i<3;i++)
            {
                for (int j = 0; j < 3;j++)
                {
                    Console.Write("\t" + array[i,j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void perulanganForEach()
        {
            Console.WriteLine("----------------------- Perulangan ForEach ----------------------\n");

            string[] array = new string[]
            {
                "Astika", "Isni", "Firdha", 
                "Ilham","Alfi","Muafa",
                "Anwar","Laudry","Marchelino",
                "Alwi", "Mario","Toni"
            };

            foreach(string s in array)
            {
                Console.WriteLine("\t"+s);
            }

            Console.WriteLine("\n-------------------------------------------------------------------\n");
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("\t"+(i+1)+". " + array[i]);
            }

        }

        static void array()
        {
            Console.WriteLine("----------------------- Array ----------------------\n");
            
            int[] staticIntArray = new int[3];

            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;

            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);

            Console.WriteLine("\n-----------------------------------------------------\n");
        }

        static void array2Dimensi()
        {
            int[,] array = new int[,]
            {
                { 1, 2, 3 },
                { 3, 5, 6 },
                { 7, 8, 9 }
            };

            Console.WriteLine(array[0,0]);
            Console.WriteLine(array[0,1]);
            Console.WriteLine(array[0,2]);
            Console.WriteLine(array[1,0]);
            Console.WriteLine(array[1,1]);
            Console.WriteLine(array[1,2]);
            Console.WriteLine(array[2,0]);
            Console.WriteLine(array[2,1]);
            Console.WriteLine(array[2,2]);
        }

        static void splitJoin()
        {
            Console.WriteLine("------------------------------- Split dan Join --------------------------------\n");

            Console.Write("\tMasukan kalimat yang kamu coba: ");
            string kalimat = Console.ReadLine();
            string[] kal = kalimat.Split(" ");

            foreach(string kata in kal)
            {
                Console.WriteLine("\t- "+kata);
            }

            Console.Write("\t");
            Console.WriteLine(string.Join("~", kal));

            Console.WriteLine("\n------------------------------------------------------------------------------\n");
        }

        static void subString()
        {
            Console.WriteLine("---------------------------------- Substring ----------------------------------\n");
            Console.Write("Masukan kalimat: ");
            string kal = Console.ReadLine();

            Console.WriteLine("Substring(1,4) : " + kal.Substring(1, 4));
            Console.WriteLine("Substring(5,2) : " + kal.Substring(5, 2));
            Console.WriteLine("Substring(7,9) : " + kal.Substring(7, 9));
            Console.WriteLine("Substring(9) : " + kal.Substring(9));

            Console.WriteLine("\n------------------------------------------------------------------------------\n");
        }

        static void convertToCharArray()
        {
            Console.WriteLine("------------------------------- Convert to Char Array --------------------------------\n");
            Console.Write("Masukan kalimat : ");
            string kal = Console.ReadLine();

            char[] array = kal.ToCharArray();

            foreach(char ch in array)
            {
                Console.WriteLine("\t"+ch);
            }

            Console.WriteLine("\n-------------------------------------------------------------------------------------\n");
        }


    }
}
