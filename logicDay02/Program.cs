﻿using System;
using System.Xml.Schema;

namespace logicDay02
{
    internal class Program
    {
        static void Main(string[] args)
        {

            /*ifStatement();
            ifElseStatement();
            ifNested();
            ternary();
            switchCase();
            contohIfNested();
            lingkaran();*/
            Console.ReadKey();
        }

        static void ifStatement()
        {
            Console.WriteLine("~~~~~If Statement~~~~~~~");
            int x, y;
            Console.Write("Masukan nilai x: ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai y: ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai x lebih besar dari nilai y");
            }
            if (x < y)
            {
                Console.WriteLine("Nilai x lebih kecil dari nilai y");
            }
            if (x == y)
            {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }

        }

        static void ifElseStatement()
        {
            Console.WriteLine("~~~~~ If Else Statement ~~~~~");
            int  nilai;
            Console.Write("Masukan nilaimu: ");
            nilai = int.Parse(Console.ReadLine());


            if (nilai>=70 && nilai <=100)
            {
                Console.WriteLine("Nilaimu Bagus, Pertahankan ya!.");
            }
            else if (nilai<=69 && nilai >=0)
            {
                Console.WriteLine("Nilaimu kurang, Tingkatkan lagi ya. :)");
            }
            else
            {
                Console.WriteLine("Opss.. angka yang kamu masukan salah.");
            }

        }

        static void ifNested()
        {
            Console.WriteLine("~~~~~~ If Nested ~~~~~");
            Console.Write("Masukan nilaimu: ");
            int nilai = int.Parse((string)Console.ReadLine());

            if(nilai >= 70)
            {
                Console.WriteLine("Selamat kamu lulus");
                if (nilai == 100)
                {
                    Console.WriteLine("Nilaimu sempurna");
                }
            }
            else
            {
                Console.WriteLine("Yaah nilaimu kurang, belajar lagi ya!!");
            }

        }

        static void ternary()
        {
            Console.WriteLine("~~~~~ Ternary ~~~~~");
            int x, y;
            Console.Write("Masukan nilai x : ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai y: ");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Nilai x lebih besar dari nilai y" : x < y ? "Nilai x lebih kecil dari nilai y" : "Nilai x sama dengan nilai y";

            Console.WriteLine(hasilTernary);

        }

        static void contohIfNested()
        {
            Console.WriteLine("~~~~~ Contoh Menjual Pakaian ~~~~~");
            Console.Write("Masukan kode baju: ");
            int kodeBaju = int.Parse((string)Console.ReadLine());
            Console.Write("Masukan ukuran baju: ");
            string ukBaju = Console.ReadLine();

            int harga=0;
            string merkBaju;

            if (kodeBaju==1)
            {
                merkBaju = "IMP";
                if (ukBaju == "S")
                {
                    harga = 200000;
                }
                else if (ukBaju=="M")
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kodeBaju==2)
            {
                merkBaju = "Prada";
                if (ukBaju == "S")
                {
                    harga = 150000;
                }
                else if (ukBaju == "M")
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else
            {
                merkBaju = "GUCCI";
                harga = 200000;
            }

            Console.WriteLine("Merk baju yang dipilih: " + merkBaju);
            Console.WriteLine("Harga baju yang dipilih: " + harga);
        }

        static void switchCase()
        {
            Console.WriteLine("~~~~~ Switch Case ~~~~~");
            Console.Write("Pilihlah buah yang kamu sukai (apel/mangga/anggur): ");
            string buah = Console.ReadLine().ToLower();

            switch(buah)
            {
                case "apel":
                    Console.WriteLine("Oh kamu suka buah apel, apel memang enak");
                    break;
                case "mangga":
                    Console.WriteLine("Oh kamu suka buah mangga, mangga memang enak");
                    break;
                case "anggur":
                    Console.WriteLine("Oh kamu suka anggur, anggur memang enak");
                    break;
                default:
                    Console.WriteLine("Gimana sih, kan ga ada pilihannya wkwkwk");
                    break;
            }
        }

        static double kelilingLingkaran(double r)
        {
            double phi = 3.14d;
            double kLingkaran = 2 * phi * r;
            return kLingkaran;
        }

        static double luasLingkaran(double r)
        {
            double phi = 3.14;
            double lLingkaran = phi * r * r;
            return lLingkaran;
        }

        static int kpersegi(int s)
        {
            return 4*s;
        }

        static int lpersegi(int s)
        {
            return s * s;
        }

        static void lingkaran ()
        {
            Console.WriteLine("~~~~~ Mari berhitung lingkaran ~~~~~");
            Console.WriteLine("Mari hitung keliling lingkaran.");
            Console.Write("Masukan jari-jari: ");
            double rk = int.Parse(Console.ReadLine());
            double kelLingkaran = kelilingLingkaran(rk);
            Console.WriteLine("Keliling lingkarannya adalah: "+kelLingkaran);


            Console.WriteLine("Mari hitung luas lingkaran.");
            Console.Write("Masukan jari-jari: ");
            double rl = int.Parse(Console.ReadLine());
            double lLingkaran = luasLingkaran(rl);
            Console.WriteLine("Luas Lingkarannya adalah: " + lLingkaran);

            Console.WriteLine("\n~~~~~ Mari berhitung Persegi ~~~~~");
            Console.WriteLine("Mari hitung keliling Persegi.");
            Console.Write("Masukan panjang sisi: ");
            int  sk = int.Parse(Console.ReadLine());
            double kelPersegi = kpersegi(sk);
            Console.WriteLine("Keliling lingkarannya adalah: " + kelPersegi);


            Console.WriteLine("Mari hitung luas lingkaran.");
            Console.Write("Masukan jari-jari: ");
            int sl = int.Parse(Console.ReadLine());
            double luasPersegi = lpersegi(sl);
            Console.WriteLine("Luas Lingkarannya adalah: " + luasPersegi);

        }




    }
}
