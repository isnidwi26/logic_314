			/******* TUGAS HARI KE-3 ******/

CREATE DATABASE DB_Sales
USE DB_Sales

create table SALESPERSON
(
ID int primary key identity(1,1),
NAME varchar(50),
BOD date not null,
SALARY decimal(18,2) not null
)
insert into SALESPERSON
values
('Abe', '1988/11/9', 140000),
('Bob', '1978/11/9', 44000),
('Chris', '1983/11/9', 40000),
('Dan', '1980/11/9', 52000),
('Ken', '1977/11/9', 115000),
('Joe', '1990/11/9', 38000)

create table ORDERS
(
ID int primary key identity(1,1),
ORDER_DATE date not null,
CUST_ID int null,
SALESPERSON_ID int not null,
AMOUNT decimal(18,2) not null
)
insert into ORDERS
values
('2020/8/2', 4, 2, 540),
('2021/1/22', 4, 5, 1800),
('2019/7/14', 9, 1, 460),
('2018/1/29', 7, 2, 2400),
('2021/2/3', 6, 4, 600),
('2020/3/2', 6, 4, 720),
('2021/5/6', 9, 4, 150)

--Bagian A
select sp.NAME, COUNT(ord.ID) [Jumlah order]
from SALESPERSON as sp
join ORDERS as  ord on sp.ID = ord.SALESPERSON_ID
group by sp.NAME
having count(ord.ID) > 1

--Bagian B
select sp.NAME, SUM(ord.AMOUNT) [Jumlah order]
from SALESPERSON as sp
join ORDERS as  ord on sp.ID = ord.SALESPERSON_ID
group by sp.NAME
having SUM(ord.AMOUNT) > 1000

--Bagian C
select sp.NAME [NAMA], DATEDIFF(YEAR,sp.BOD,GETDATE()) as [UMUR], sp.SALARY [GAJI], SUM(ord.AMOUNT) [TOTAL AMOUNT ORDER] 
from SALESPERSON as sp
join ORDERS as ord on sp.ID = ord.SALESPERSON_ID
WHERE YEAR(ORDER_DATE) >= 2020
group by NAME, SALARY, BOD
order by UMUR 

--Bagian D
select  AVG(ORD.AMOUNT) [AMOUNT AVERAGE], SP.NAME [NAME SALES]
from ORDERS ORD
join SALESPERSON SP on ORD.SALESPERSON_ID = SP.ID
group by SP.NAME
order by [AMOUNT AVERAGE] desc

--Bagian E
select SP.NAME [NAME SALES], COUNT(ORD.ID) [TOTAL ORDER], SUM(ORD.AMOUNT) [TOTAL_AMOUNT], SP.SALARY*0.3 [BONUS]
from SALESPERSON SP
join ORDERS ORD on ORD.SALESPERSON_ID = SP.ID
group by SP.NAME, SP.SALARY
having COUNT(ORD.ID) > 2 and SUM(ORD.AMOUNT) > 1000

--Bagian F
select SP.NAME
from SALESPERSON SP
left join ORDERS ORD on SP.ID = ORD.SALESPERSON_ID
where ORD.SALESPERSON_ID is null

--Bagian G
select sp.NAME, sp.SALARY [GAJI KOTOR], sp.SALARY * 0.02 [POTONGAN GAJI], sp.SALARY-(sp.SALARY*0.02) [GAJI BERSIH]
from SALESPERSON sp
left join ORDERS ord on sp.ID = ord.SALESPERSON_ID
where ord.SALESPERSON_ID is null 