			/**** Tugas SQL Hari ke-2 ****/

CREATE DATABASE DB_Entertainer
use DB_Entertainer

create table artis
(
kd_artis varchar(100) primary key,
nm_artis varchar(100),
jk varchar(100),
bayaran bigint,
award int,
negara varchar(100)
)
insert into artis (kd_artis,nm_artis,jk,bayaran,award,negara)
values
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 2, 'ID')


create table film
(
kd_film varchar(10) primary key,
nm_film varchar(55),
genre varchar(55),
artis varchar(55),
produser varchar(55),
pendapatan bigint,
nominasi int
)
insert into film
values
('F001','IRON MAN','G001','A001','PD01', 2000000000, 3),
('F002','IRON MAN 2','G001','A001','PD01', 1800000000, 2),
('F003','IRON MAN 3','G001','A001','PD01', 1200000000, 0),
('F004','AVENGER : CIVIL WAR','G001','A001','PD01', 2000000000, 1),
('F005','SPIDERMAN HOME COMING','G001','A001','PD01', 1300000000, 0),
('F006','THE RAID','G001','A004','PD03', 800000000, 5),
('F007','FAST & FARIOUS','G001','A004','PD05', 830000000, 2),
('F008','HABIBIE DAN AINUN','G004','A005','PD03', 670000000, 4),
('F009','POLICE STORY','G001','A003','PD02', 700000000, 3),
('F010','POLICE STORY 2','G001','A003','PD02', 710000000, 1),
('F011','POLICE STORY 3','G001','A003','PD02', 615000000, 0),
('F012','RUSH HOUR','G003','A003','PD05', 695000000, 2),
('F013','KUNGFU PANDA','G003','A003','PD05', 923000000, 5)

create table produser
(
kd_produser varchar(50) primary key,
nm_produser varchar(50),
internasional varchar(50)
)
insert into produser
values
('PD01','MARVEL', 'YA'),
('PD02','HONGKONG CINEMA', 'YA'),
('PD03','RAPI FILM', 'TIDAK'),
('PD04','PARKIT', 'TIDAK'),
('PD05','PARAMOUNT PICTURE', 'YA')

create table negara
(
kd_negara varchar(100) primary key,
nm_negara varchar(100)
)
insert into negara
values
('AS','AMERIKA SERIKAT'),
('HK','HONGKONG'),
('ID','INDONESIA'),
('IN','INDIA')

create table genre
(
kd_genre varchar(50) primary key,
nm_genre varchar(50)
)
insert into genre
values
('G001','ACTION'),
('G002','HORROR'),
('G003','COMEDY'),
('G004','DRAMA'),
('G005','THRILLER'),
('G006','FICTION')

---PANGGIL
select * from artis
select * from film
select * from produser
select * from negara

---NO 1
select produser.nm_produser, SUM(film.pendapatan) as pendapatan
from produser
join film on produser.kd_produser = film.produser
where nm_produser='marvel'
group by produser.nm_produser

---NO 2
select nm_film, nominasi from film
where nominasi = 0

---NO 3
select nm_film from film where nm_film like 'p%'

---NO 4
select nm_film from film where nm_film like '%y'

---NO 5
select nm_film from film where nm_film like '%d%'

---NO 6
select film.nm_film, artis.nm_artis 
from film 
join artis on film.artis = artis.kd_artis

---NO 7
select film.nm_film, negara.kd_negara as negara
from film
join artis on film.artis = artis.kd_artis
join negara on artis.negara = negara.kd_negara
where negara.nm_negara = 'HONGKONG'

---atau
select film.nm_film, negara as negara
from film
join artis on film.artis = artis.kd_artis
where negara = 'hk'

---NO 8
select film.nm_film, negara.nm_negara
from film
join artis on film.artis = artis.kd_artis
join negara on artis.negara = negara.kd_negara
where negara.nm_negara not like '%o%' 

---NO 9
select nm_artis 
from artis
left join film on film.artis = artis.kd_artis
where film.nm_film is null


---NO 10 
select nm_artis, nm_genre
from artis
join film on artis.kd_artis = film.artis
join genre on film.genre = genre.kd_genre
where nm_genre = 'DRAMA'

---NO 11
select distinct nm_artis, nm_genre
from artis
join film on artis.kd_artis = film.artis
join genre on film.genre = genre.kd_genre
where nm_genre = 'ACTION'

---NO 12
select kd_negara, nm_negara, COUNT(film.nm_film) as [jumlah film]
from negara
left join artis on negara.kd_negara = artis.negara
left join film on artis.kd_artis = film.artis
group by kd_negara,nm_negara
order by nm_negara asc

---atau
select kd_negara, nm_negara, COUNT(film.nm_film) as [jumlah film]
from negara
join artis on negara.kd_negara = artis.negara
join film on artis.kd_artis = film.artis
group by kd_negara,nm_negara


---NO 13
select nm_film
from film
join produser on film.produser = produser.kd_produser
where produser.internasional = 'YA'

---NO 14
select nm_produser, count(produser) as [jumlah film]
from produser
left join film on produser.kd_produser = film.produser
group by nm_produser

