CREATE DATABASE DB_HR
USE DB_HR



create table tb_karyawan
(
id bigint primary key identity(1,1),
nip varchar(50) not null,
nama_depan varchar(50) not null,
nama_belakang varchar(50) not null,
jenis_kelamin varchar(50) not null,
agama varchar(50) not null,
tempat_lahir varchar(50) not null,
tgl_lahir date,
alamat varchar(100) not null,
pendidikan_terakhir varchar(50) not null,
tgl_masuk date
)
insert into tb_karyawan
values
('001', 'Hamidi','Samsudin','Pria','Islam','Sukabumi','1977/04/21', 'Jl. Sudirman No.12', 'S1 Teknik Mesin','2015/12/07'),
('002', 'Ghandi','Wamida','Wanita','Islam','Palu','1992/01/12', 'Jl. Rambutan No.22', 'SMA Negeri 02 Palu','2014/12/01'),
('003', 'Paul','Chritian','Pria','Kristen','Ambon','1980/05/27', 'Jl. Veteran No.4', 'S1 Pendidikan Geografi','2014/01/12')

create table tb_divisi
(
id bigint primary key identity(1,1) not null,
kd_divisi varchar(50) not null,
nama_divisi varchar(50) not null
)
insert into tb_divisi
values
('GD', 'Gudang'),
('HRD','HRD'),
('KU','Keuangan'),
('UM', 'Umum')

create table tb_jabatan
(
id bigint primary key identity(1,1) not null,
kd_jabatan varchar(50) not null,
nama_jabatan varchar(50) not null,
gaji_pokok numeric null,
tunjangan_jabatan numeric null
)
insert into tb_jabatan
values
('MGR', 'Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WMGR', 'Wakil Manager', 4000000, 1200000)


create table tb_pekerjaan
(
id bigint primary key identity(1,1) not null,
nip varchar(50) not null,
kode_jabatan varchar(50) not null,
kode_divisi varchar(50) not null,
tunjangan numeric null,
kota_penempatan varchar(50) null
)
insert into tb_pekerjaan
values
('001','ST','KU',750000,'Cianjur'),
('002','OB','UM',350000,'Sukabumi'),
('003','MGR','HRD',1500000,'Sukabumi')


---No 1
select CONCAT(karyawan.nama_depan, ' ', karyawan.nama_belakang) nama_lengkap, jabatan.nama_jabatan, jabatan.gaji_pokok+jabatan.tunjangan_jabatan gaji_tunjangan
from tb_karyawan karyawan
join tb_pekerjaan pekerjaan on karyawan.nip = pekerjaan.nip
join tb_jabatan jabatan on pekerjaan.kode_jabatan = jabatan.kd_jabatan
where jabatan.gaji_pokok+jabatan.tunjangan_jabatan < 5000000

--No 2
select CONCAT(kar.nama_depan, ' ', kar.nama_belakang) nama_lengkap, jb.nama_jabatan, div.nama_divisi, 
	jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan as [Total gaji], 
	(jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 0.05 as Pajak, 
	(jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan)-((jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 0.05) as [Gaji bersih]
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_divisi div on pkr.kode_divisi = div.kd_divisi
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
where pkr.kota_penempatan not like 'Sukabumi'

--No 3 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus (bonus=25% dari total gaji(gaji pokok+tunjangan_jabatan+tunajangan_kinerja) * 7
select kar.nip, CONCAT(kar.nama_depan, ' ', kar.nama_belakang) nama_lengkap, jb.nama_jabatan, div.nama_divisi,
		(((jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 7) *0.25) as Bonus
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_divisi div on pkr.kode_divisi = div.kd_divisi
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
order by kar.jenis_kelamin asc

--No 4 Tampilkan nama lengkap, ttal gaji, infak(5%*total gaji) yang mempunyai jabatan MGR
select kar.nip, CONCAT(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap, jb.nama_jabatan, div.nama_divisi , jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan as total_gaji, (jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 0.05 as Infak
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
join tb_divisi div on pkr.kode_divisi = div.kd_divisi
where pkr.kode_jabatan = 'MGR'

--No 5 Tampilkan nama lengkap, nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), 
-- dan total gaji(gapok+tjabatan+tpendidikan) dimana pendidikan akhirnya adalah S1
select kar.nip, CONCAT(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap, jb.nama_jabatan, kar.pendidikan_terakhir,  CAST(2000000 as int) as tunjangan_pendidikan, jb.gaji_pokok+jb.tunjangan_jabatan+2000000 as total_gaji
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
where kar.pendidikan_terakhir like 's1%'
order by nip asc

--No 6 Tampilkan nip, nama lengkap, jabatan, nama divisi, bonus
select kar.nip, CONCAT(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap, 
		jb.nama_jabatan, div.nama_divisi, jb.gaji_pokok, jb.tunjangan_jabatan, pkr.tunjangan,
case	
	when jb.kd_jabatan = 'MGR' then ((jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 7) * 0.25
	when jb.kd_jabatan = 'ST' then  ((jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 5) * 0.25
	else 0.25 * ((jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan) * 2)
end as Bonus
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
join tb_divisi div on pkr.kode_divisi = div.kd_divisi
order by jenis_kelamin asc

--No 7
alter table tb_karyawan add constraint unique_nip unique(nip)

--No 8
create index indeks_nip on tb_karyawan(nip)

--No 9 Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan 
-- kondisi nama belakang di awali dengan huruf W
select
case 
	when nama_belakang like 'w%' then nama_depan + ' ' + upper(nama_belakang)
	else nama_depan + ' ' + nama_belakang
end as Nama_lengkap
from tb_karyawan

--No 10
select CONCAT(kar.nama_depan, ' ', kar.nama_belakang) as nama_lengkap, tgl_masuk, jb.nama_jabatan, div.nama_divisi,  
		jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan as total_gaji, (jb.gaji_pokok+jb.tunjangan_jabatan+pkr.tunjangan)*0.1 as bonus,
		DATEDIFF(YEAR,tgl_masuk, getdate()) as lama_bekerja
from tb_karyawan kar
join tb_pekerjaan pkr on kar.nip = pkr.nip
join tb_jabatan jb on pkr.kode_jabatan = jb.kd_jabatan
join tb_divisi div on pkr.kode_divisi = div.kd_divisi
WHERE DATEDIFF(YEAR,tgl_masuk, getdate()) >= 8


