﻿using System;

namespace logicDay05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*contain();
            padLeftRight();
            convertArray();*/
            Console.ReadKey();
        }

        static void convertArray()
        {
            Console.WriteLine("-------------------------------- Convert Array --------------------------------");
            Console.Write("Masukkan angka array (pakai koma): ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            //untuk misal join
            Console.WriteLine(string.Join(":", array));


            Console.WriteLine(new string('-', 80));
        }

        static void contain()
        {
            Console.WriteLine("-------------------------------- Contain --------------------------------");

            Console.Write("Masukkan kalimat: ");
            string kalimat = Console.ReadLine();
            Console.Write("Masukkan contain: ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat {kalimat} ini mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat '{kalimat}' ini tidak mengandung {contain}");
            }

            Console.WriteLine(new string('-', 80));
        }
    
        static void padLeftRight()
        {
            Console.WriteLine("-------------------------------- Pad Left or Right --------------------------------");

            Console.Write("Masukkan input: ");
            int input = int.Parse(Console.ReadLine());
            Console.Write("Masukkan panjang karakter: ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukkan char: ");
            char chars =char.Parse(Console.ReadLine());


            Console.WriteLine("Hasil Pad Left : " + (input.ToString().PadLeft(n, chars)));

        }
    
    }
}
