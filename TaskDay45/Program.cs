﻿using System;
using System.ComponentModel;

namespace TaskDay45
{
    internal class Program
    {
        static void Main(string[] args)
        {
            menu();
            Console.ReadKey();
        }

        static void menu()
        {
            Console.WriteLine("--------------- Selamat datang di kumpulan tugas hari ke 4-5 ---------------");
            Console.WriteLine("\t1. Gaji Karyawan");
            Console.WriteLine("\t2. Mehitung kata pada kalimat");
            Console.WriteLine("\t3. Mengganti huruf pada kata kecuali awal dan akhir");
            Console.WriteLine("\t4. Mengganti huruf pada kata diawal dan diakhir");
            Console.WriteLine("\t5. Menghapus huruf pada awal kata");
            Console.WriteLine("\t6. Replace angka");
            Console.WriteLine("\t7. Menambahkan simbol pada awal kata");
            Console.WriteLine("\t8. Fibonacci");
            Console.WriteLine("\t9. Konversi waktu");
            Console.WriteLine("\t10. Menjual pakaian");
            Console.WriteLine("\t11. Membeli baju lebaran");
            Console.WriteLine("\t12. Membuat persegi dengan angka dan '*'");


            Console.Write("\n\tSilahkan pilih menu: ");
            int pilih = int.Parse(Console.ReadLine());
            Console.Clear();

            switch (pilih)
            {
                case 1: tugas1(); break;
                case 2: tugas2(); break;
                case 3: tugas3(); break;
                case 4: tugas4(); break;
                case 5: tugas5(); break;
                case 6: tugas6(); break;
                case 7: tugas7(); break;
                case 8: tugas8(); break;
                case 9: tugas9(); break;
                case 10: tugas10(); break;
                case 11: tugas11(); break;
                case 12: tugas12(); break;
                default:
                    Console.WriteLine("\tPunten ga ada pilihannya, coba lagi yes!\n");
                    menu();
                    break;
            }
        }

        static void tugas1()
        {
            Console.WriteLine("-------------------------------- Gaji Karyawan --------------------------------");
            tugas1:
            Console.Write("\tGolongan\t: ");
            int golongan = int.Parse(Console.ReadLine());
            Console.Write("\tJam Kerja\t: ");
            int jamKerja = int.Parse(Console.ReadLine());
            double upah = 0, upahPerjam = 0, upahLembur = 0, waktuLembur = 0, total = 0;
            if (golongan == 1 && jamKerja > 0)
            {
                upahPerjam = 2000;
                if (jamKerja > 40)
                {
                    waktuLembur = jamKerja - 40;
                    upah = upahPerjam * (jamKerja - waktuLembur);
                    upahLembur = waktuLembur * 1.5d * upahPerjam;
                }
                else
                {
                    upah = upahPerjam * jamKerja;
                }

                total = upah + upahLembur;

            }
            else if (golongan == 2 && jamKerja > 0)
            {
                upahPerjam = 3000;
                if (jamKerja > 40)
                {
                    waktuLembur = jamKerja - 40;
                    upah = upahPerjam * (jamKerja - waktuLembur);
                    upahLembur = waktuLembur * 1.5d * upahPerjam;
                }
                else
                {
                    upah = upahPerjam * jamKerja;
                }
                total = upah + upahLembur;
            }
            else if (golongan == 3 && jamKerja > 0)
            {
                upahPerjam = 4000;
                if (jamKerja > 40)
                {
                    waktuLembur = jamKerja - 40;
                    upah = upahPerjam * (jamKerja - waktuLembur);
                    upahLembur = waktuLembur * 1.5d * upahPerjam;
                }
                else
                {
                    upah = upahPerjam * jamKerja;
                }
                total = upah + upahLembur;
            }
            else if (golongan == 4 && jamKerja > 0)
            {
                upahPerjam = 5000;
                if (jamKerja > 40)
                {
                    waktuLembur = jamKerja - 40;
                    upah = upahPerjam * (jamKerja - waktuLembur);
                    upahLembur = waktuLembur * 1.5d * upahPerjam;
                }
                else
                {
                    upah = upahPerjam * jamKerja;
                }
                total = upah + upahLembur;
            }
            else
            {
                Console.WriteLine("\tGolongan tidak valid. Silahkan input data dengan benar!");
                Console.WriteLine(new string('-', 80));
                goto tugas1;
            }

            Console.WriteLine("\tUpah\t\t: " + upah);
            Console.WriteLine("\tLembur\t\t: " + upahLembur);
            Console.WriteLine("\tTotal\t\t: " + total);
            Console.WriteLine(new string('-', 80));

        }

        static void tugas2()
        {

            Console.WriteLine("-------------------------------- Menghitung kata pada kalimat --------------------------------");
            Console.Write("\tMasukan kalimat yang akan dihitung: ");
            string[] kata = Console.ReadLine().Split(' ');

            for (int i = 0; i < kata.Length; i++)
            {
                Console.WriteLine("\tKata ke-" + (i + 1) + " " + kata[i]);
            }

            Console.WriteLine("\tTotal kata adalah " + kata.Length);


            Console.WriteLine(new string('-', 94));
        }

        static void tugas3()
        {
            Console.WriteLine("--------------- Mengganti huruf pada kata kecuali awal dan akhir ---------------");
            Console.Write("\tMasukkan kalimat: ");
            string[] kal = Console.ReadLine().Split(' ');

            for (int i = 0; i < kal.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kal[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(kal[i][j]);
                    }
                    else if (j == kal[i].Length - 1)
                    {
                        Console.Write(kal[i][j]);
                    }
                    else
                    {
                        Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                }
                Console.Write(" ");
            }
            Console.WriteLine();
            Console.WriteLine(new string('-', 80));
        }

        static void tugas4()
        {
            Console.WriteLine("----------------- Mengganti huruf pada kata diawal dan diakhir -----------------");
            Console.Write("\tMasukkan kalimat: ");
            string kalimat = Console.ReadLine();
            string[] kal = kalimat.Split(' ');

            string tampung = "";

            //CARA 1 DAN 2
            for (int i = 0; i < kal.Length; i++)
            {
                Console.Write("\t");
                for (int j = 0; j < kal[i].Length; j++)
                {
                    if (j == 0)
                    {
                        tampung += "*";
                        //Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                    else if (j == kal[i].Length - 1)
                    {
                        tampung += "*";
                        //Console.Write(kal[i].Replace(kal[i], "*"));
                    }
                    else
                    {
                        tampung += kal[i][j];
                        //Console.Write(kal[i][j]);

                    }

                }
                tampung += " ";
                //Console.Write(" ");
            }
            Console.WriteLine(tampung);

            Console.WriteLine();
            Console.WriteLine(new string('-', 80));
        }

        static void tugas5()
        {
            Console.WriteLine("----------------- Menghapus huruf pada awal kata -----------------");
            Console.Write("\tMasukkan kalimat: ");
            string[] kal = Console.ReadLine().Split(' ');

            Console.Write("\t");
            for (int i = 0; i < kal.Length; i++)
            {
                Console.Write(kal[i].Remove(0,1)+" ");            
            }

            Console.WriteLine();
            Console.WriteLine(new string('-', 80));
        }

        static void tugas6()
        {
            Console.WriteLine("------------------------------- Replace angka --------------------------------");
            Console.Write("\tMasukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());
            Console.Write("\tMasukan angka: ");
            int angka = int.Parse(Console.ReadLine());
            Console.Write("\tMasukan rasio: ");
            int rasio = int.Parse(Console.ReadLine());
            
            //CARA 1
            /*for (int i = 1; i <= range; i++)
            {
                angka *= rasio;
                string convAngka = angka.ToString();

                if (i % 2 == 0)
                {

                    Console.Write(" " + convAngka.Replace(convAngka, "*") + " ");
                }
                else
                {
                    Console.Write(" " + convAngka + " ");
                }
            }*/

            //CARA 2
            string tampung = "";
            for (int i = 1; i <= range; i++)
            {
                
                string convAngka = angka.ToString();

                if (i % 2 == 0)
                {
                    tampung += " * ";
                }
                else
                {
                    tampung += " " + convAngka + " ";
                }
                angka *= rasio;
            }

            Console.WriteLine("\n\t\t\t" + tampung);

            Console.WriteLine();
            Console.WriteLine(new string('-', 78));
        }

        static void tugas7()
        {

            Console.WriteLine("----------------- Menambahkan simbol pada awal kata -----------------");
            Console.Write("\tMasukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());
            Console.Write("\tMasukkan angka: ");
            int angka = int.Parse(Console.ReadLine());
            
            //CARA 1
            /*for (int i = 1; i <= range; i++)
            {
                Console.Write("\t");
                angka += 5;
                string convAngka = angka.ToString();
                if (i % 2 == 1)
                {
                    Console.Write(convAngka.Insert(0, "-") + " ");
                }
                else
                {
                    Console.Write(" " + convAngka + " ");
                }

            }*/

            //CARA 2
            /*string tampung = "";
            for (int i = 1; i <= range; i++)
            {
                Console.Write("\t");
                angka += 5;
                string convAngka = angka.ToString();
                if (i % 2 == 1)
                {
                    tampung += "-" + convAngka;
                }
                else
                {
                    tampung += " " + convAngka + " ";
                }
            }
            Console.WriteLine("\n\t\t\t" + tampung);*/


            //CARA 3
            for (int i = 1; i <= range; i++)
            {
                Console.Write("\t");

                
                if (i%2==1)
                {
                    Console.Write(angka * -1);

                }
                else
                {
                    //angka = Math.Abs(angka);
                    Console.Write(angka);
                }
                angka += angka;

            }

            Console.WriteLine();
            Console.WriteLine(new string('-', 69));
        }

        static void tugas8()
        {
            Console.WriteLine("-------------------------------- Fibonacci --------------------------------");
            Console.Write("\tMasukan jumlah bilangan angka: ");
            int range = int.Parse(Console.ReadLine());
            

            

            //CARA 1
            int a = 1, b = 1, angkaFib;
            for (int i = 0; i < range; i++)
            {
                Console.Write("\t");
                if (i <= 1)
                {
                    angkaFib = 1;
                }
                else
                {
                    angkaFib = a + b;
                    a = b;
                    b = angkaFib;
                }
                Console.Write(angkaFib + " ");
            }
            Console.WriteLine();

            //CARA 2
            int[] num = new int[range];
            for (int i = 0; i < range; i++)
            {
                if (i <= 1)
                {
                    num[i] = 1;
                }
                else
                {
                    num[i] = num[i-2] + num[i-1];
                }
                //Console.Write(num[i] + " ");
            }
            Console.Write("\t");
            Console.Write(string.Join("\t",num));



            Console.WriteLine();
            Console.WriteLine(new string('-', 75));
        }

        static void tugas9()
        {
            Console.WriteLine("-------------------------------- Konversi Waktu --------------------------------");
            Console.Write("\tMasukkan waktu sekarang: ");
            string waktu = Console.ReadLine();

            string PM = waktu.Substring(8, 2);
            int convWaktu = int.Parse(waktu.Substring(0, 2));

            if (convWaktu <= 12)
            {

                if (PM == "PM")
                {

                    convWaktu += 12;
                    Console.WriteLine("\t" + convWaktu.ToString() + waktu.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine("\t" + waktu.Substring(0, 8));
                }

            }
            else
            {
                Console.WriteLine("\tSilahkan input waktu yang benar");
            }
            Console.WriteLine();
            Console.WriteLine(new string('-', 80));
        }

        static void tugas10()
        {
            Console.WriteLine("----------------------------- Menjual Pakaian -----------------------------");
            Console.Write("\tMasukan kode baju: ");
            string kodeBaju = Console.ReadLine();
            Console.Write("\tMasukan ukuran baju: ");
            string ukBaju = Console.ReadLine().ToUpper();

            int harga = 0;
            string merkBaju = "";

            if (kodeBaju == "1" || kodeBaju == "2" || kodeBaju == "3")
            {
                if (kodeBaju == "1")
                {
                    merkBaju = "IMP";
                    if (ukBaju == "S")
                    {
                        harga = 200000;
                    }
                    else if (ukBaju == "M")
                    {
                        harga = 220000;
                    }
                    else
                    {
                        harga = 250000;
                    }
                }
                else if (kodeBaju == "2")
                {
                    merkBaju = "Prada";
                    if (ukBaju == "S")
                    {
                        harga = 150000;
                    }
                    else if (ukBaju == "M")
                    {
                        harga = 160000;
                    }
                    else
                    {
                        harga = 170000;
                    }
                }
                else if (kodeBaju == "3")
                {
                    merkBaju = "GUCCI";
                    harga = 200000;
                }
                Console.WriteLine("\tMerk baju yang dipilih: " + merkBaju);
                Console.WriteLine("\tHarga baju yang dipilih: " + harga);
            }
            else
            {
                Console.WriteLine("\tData yang anda masukkan salah");
            }

            


            Console.WriteLine(new string('-', 75));
        }

        static void tugas11()
        {
            int maks = 0;
            Console.WriteLine("-------------------------------- Belanja Lebaran -------------------------------");
            Console.Write("\tUang kamu\t: ");
            int uang = int.Parse(Console.ReadLine());

            /*
            //CARA ISNI
            Console.Write("\tHarga Baju\t: ");
            string[] hBaju = Console.ReadLine().Split(',');
            Console.Write("\tHarga Celana\t: ");
            string[] hCelana = Console.ReadLine().Split(',');

            int[] totalHarga = new int[hBaju.Length];
            
            Console.WriteLine(new string('-', 80));
            Console.WriteLine("\tRincian dan Total dari Harga Baju dan Harga Celana ");

            for (int i = 0; i < hBaju.Length; i++)
            {
                totalHarga[i] = int.Parse(hBaju[i]) + int.Parse(hCelana[i]);
                Console.WriteLine("\t" + hBaju[i] + " + " + hCelana[i] + " = " + totalHarga[i]);
            }
            //Pengurutan data dari yang tertinggi
            for (int i = 0; i < totalHarga.Length; i++)
            {
                for (int j = 0; j < totalHarga.Length; j++)
                {
                    if (totalHarga[i] > totalHarga[j])
                    {
                        maks = totalHarga[i];
                        totalHarga[i] = totalHarga[j];
                        totalHarga[j] = maks;
                    }
                }
            }

            //Mencari nilai yang memenuhi kasus
            for (int i = 0; i < totalHarga.Length; i++)
            {
                if (totalHarga[i] <= uang)
                {
                    maks = totalHarga[i];
                    break;
                }
            }*/

            //CARA 2
            Console.Write("\tHarga Baju\t: ");
            int[] hBaju = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);
            Console.Write("\tHarga Celana\t: ");
            int[] hCelana = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);

            for(int i = 0; i < hBaju.Length; i++)
            {
                for(int j = 0; j < hCelana.Length; j++)
                {
                    int totalHarga = hBaju[i] + hCelana[j];
                    if (totalHarga <= uang && totalHarga >= maks)
                    {
                        maks = totalHarga;
                    }
                }
            }


            Console.WriteLine("\n\tKamu akan membeli baju lebaran seharga: " + maks);

            Console.WriteLine(new string('-', 80));
        }

        static void tugas12()
        {

            Console.WriteLine("-------------------------------- Persegi --------------------------------");
            Console.Write("Masukkan jumlah elemen: ");
            int elemen = int.Parse(Console.ReadLine());

            //string tampung = "";

            /* 
             int a = elemen, b = 1;
             if (elemen > 0)
             {
                 for (int i = 1; i <= elemen; i++)
                 {
                     Console.Write("\t\t");
                     for (int j = 1; j <= elemen; j++)
                     {
                         if (i == 1)
                         {
                             Console.Write(b++ + "\t");
                         }
                         else if (i == elemen)
                         {
                             Console.Write(a-- + "\t");
                         }
                         else if (j == 1 || j == elemen)
                         {
                             Console.Write("*\t");
                         }
                         else
                         {
                             Console.Write(" \t");
                         }
                     }
                     Console.WriteLine("\n");
                 }
             }
             else
             {
                 Console.WriteLine("\tInvalid");
             }
            */

            //CARA BAPA
            for (int i = 1; i <= elemen; i++)
            {
                Console.Write("\t\t");
                for (int j = 1; j <= elemen; j++)
                {
                    if (i == 1 || i == elemen)
                    {
                        if(i == elemen)
                            Console.Write(elemen - j + 1 + "\t");
                        else
                            Console.Write(j + "\t");
                    }
                    else
                    {
                        if (j == elemen || j == 1)
                            Console.Write("*\t");
                        else
                            Console.Write(" \t");
                    }
                }
                Console.WriteLine("\n");
            }
            Console.WriteLine(new string('-', 73));
        }

    }
}
