﻿using System;
using System.Globalization;

namespace logicDay09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //no1();
            //no2();
            //no3();
            bermainBintang();
        }

        static void no1() 
        {
            Console.WriteLine("\t================= Hitung Bensin Ojol =================");
            Console.WriteLine("\t1. Jarak dari Toko ke Customer 1 = 2KM");
            Console.WriteLine("\t2. Jarak dari Customer 1 ke Customer 2 = 500 M");
            Console.WriteLine("\t3. Jarak dari Customer 2 ke Customer 3 = 1.5KM");
            Console.WriteLine("\t4. Jarak dari Customer 3 ke Customer 4 = 300M");
            ngulang:
            Console.Write("\tMasukkan jarak tempuh: ");
            int tempuh = int.Parse(Console.ReadLine());

            double bensin;

            double jarak1 = 2, jarak2 = 0.5, jarak3 = 1.5, jarak4 = 0.3;

            
            if(tempuh == 1)
            {
                bensin = jarak1 / 2.5;
                Console.WriteLine($"\tJarak tempuh   : {jarak1}KM");
            }
            else if(tempuh == 2)
            {
                bensin = (jarak1 + jarak2) / 2.5;
                Console.WriteLine($"\tJarak tempuh : {jarak1}KM + {jarak2*1000}M = {jarak1+jarak2}KM");
            }
            else if(tempuh == 3)
            {
                bensin = (jarak1 + jarak2 + jarak3) / 2.5;
                Console.WriteLine($"\tJarak tempuh : {jarak1}KM + {jarak2*1000}M + {jarak3}KM = {jarak1+(jarak2/10)+jarak3}KM");
            }
            else if (tempuh == 4)
            {
                bensin = (jarak1 + jarak2 + jarak3 + jarak4) / 2.5;
                Console.WriteLine($"\tJarak tempuh : {jarak1}KM + {jarak2*1000}M + {jarak3}KM + {jarak4*1000}M = {Math.Round(jarak1+(jarak2/10)+jarak3+(jarak4/10),2)}KM");
            }
            else
            {
                Console.WriteLine("\tTidak ada pilihan. Silahkan pilih ulang.");
                Console.WriteLine("\t======================================================");
                goto ngulang;
            }

            Console.WriteLine("\tTotal Bensin yang diperlukan: " + Math.Round(bensin,MidpointRounding.ToPositiveInfinity) + " Liter");

        }

        static void no2() 
        {
            Console.WriteLine("\t================== Jumlah Bahan Membuat Kue Pukis ==================");
            Console.Write("\tMasukkan total kue pukis yang akan dibuat: ");
            int totalPukis = int.Parse(Console.ReadLine());

            double terigu = 115, gula = 190, susu = 100;

            double totalterigu, totalGula, totalSusu;

            totalterigu = terigu / 15 * totalPukis;
            totalGula = gula / 15 * totalPukis;
            totalSusu = susu /15 * totalPukis;
            Console.WriteLine($"\t{Math.Round(totalterigu)} gr Terigu");
            Console.WriteLine($"\t{Math.Round(totalGula)} gr Gula Pasir");
            Console.WriteLine($"\t{Math.Round(totalSusu)} mL Susu");

        }

        static void no3()
        {
            Console.WriteLine("\t================ Segitiga tengah kosong ================");
            Console.Write("\tMasukkan jumlah indeks: ");
            int indeks = int.Parse(Console.ReadLine());
            Console.Write("\tMasukkan nilai kelipatan: ");
            int kelipatan = int.Parse(Console.ReadLine());

            
            /*int tampung=0;
            for (int i = 0; i < indeks; i++)
            {
                Console.Write("\t\t\t");
                for (int j = 0; j < indeks; j++)
                {
                    if (j == 0)
                    {
                        tampung = kelipatan;
                        Console.Write(tampung+"\t");
                    }
                    else if (j == i && j < indeks-1)
                    {
                        tampung += kelipatan*j;
                        Console.Write(tampung+"\t");                      
                    }
                    else if (i == indeks-1)
                    {
                        tampung += kelipatan;
                        Console.Write(tampung+"\t");
                    }
                    else
                        Console.Write("\t");
                }
                Console.WriteLine("\n");
            }*/

            for(int i = 0;  i < indeks; i++)
            {
                int x = 0;
                for(int j = 0; j < indeks; j++) 
                {
                    x += kelipatan;
                    if (j == 0 || i == indeks-1 || i == j)
                        Console.Write(x + "\t");
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }


        }
    
        static void bermainBintang()
        {
            Console.WriteLine("\t================ Segitiga tengah kosong ================");
            Console.Write("\tMasukkan jumlah indeks: ");
            int indeks = int.Parse(Console.ReadLine());
            Console.Write("\tMasukkan nilai kelipatan: ");
            int kelipatan = int.Parse(Console.ReadLine());

            for (int i = 0; i < indeks; i++)
            {
                int x = 0;
                for (int j = 0; j < indeks; j++)
                {
                    x += kelipatan;
                    if (j == 0 || j == indeks - 1 || i == indeks - 1 || i == j || i == 0 || j == indeks - 1 - i)
                        Console.Write(x + "\t");
                    else
                        Console.Write("\t");
                }
                Console.WriteLine();
            }
        }
    }
}
