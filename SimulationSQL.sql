---SIMULAI SQL---

create database DB_PTXA
use DB_PTXA


create table Biodata
(
id bigint primary key identity(1,1),
first_name varchar(20),
last_name varchar(30),
dob date,
pob varchar(50),
address varchar(255),
gender varchar(1),
)
insert into Biodata
values
('soraya','rahayu','1990/12/22','Bali','Jl. Raya Kuta, Bali', 'P'),
('hanum','danuary','1990/01/02','Bandung','Jl. Berkah Ramadhan, Bandung', 'P'),
('melati','marcelia','1991/03/03','Jakarta','Jl. Mawar 3, Brebes', 'P'),
('farhan','Djokrowidodo','1989/10/11','Jakarta','Jl. Bahari Raya, Solo', 'L')


create table Employee
(
id bigint primary key identity(1,1),
biodata_id bigint,
nip varchar(5),
status varchar(10),
join_date datetime,
salary decimal(10,0)
)
insert into Employee
values
(1,'XA001','Permanen','2015/11/01',12000000),
(2,'XA002','Kontrak','2017/01/02',10000000),
(3,'XA003','Kontrak','2018/08/09',10000000)


create table Contact_Person
(
id bigint primary key identity(1,1),
biodata_id bigint,
type varchar(5),
contact varchar(100)
)
insert into Contact_Person
values
(1,'MAIL','soraya.rahayu@gmail.com'),
(1,'PHONE','085612345678'),
(2,'MAIL','hanum.danuary@gmail.com'),
(2,'PHONE','081312345678'),
(2,'PHONE','087812345678'),
(3,'MAIL','melati.marceliay@gmail.com')

create table Leave
(
id bigint primary key identity(1,1),
type varchar(10),
name varchar(100)
)
insert into Leave
values
('Reguler','Cuti Tahunan'),
('Khusus','Cuti Menikah'),
('Khusus','Cuti Haji & Umroh'),
('Khusus','Cuti Melahirkan')

create table Employee_Leave
(
id int primary key identity(1,1),
employee_id int,
period varchar(4),
regular_quota int
)
insert into Employee_Leave
values
(1,'2021',16),
(2,'2021',12),
(3,'2021',12)

create table Leave_Request
(
id bigint primary key identity(1,1),
employee_id bigint,
leave_id bigint,
start_date date,
end_date date,
reason varchar(255)
)
insert into Leave_Request
values
(1,1,'2021/10/10','2021/10/12','Liburan'),
(1,1,'2021/11/12','2021/11/15','Acara Keluarga'),
(2,2,'2021/05/05','2021/05/07','Menikah'),
(2,1,'2021/09/09','2021/09/13','Touring'),
(2,1,'2021/12/20','2021/12/23','Acara Keluarga')

---JAWABAN SOAL---
---NO 1
select top 1 CONCAT(bio.first_name,' ',bio.last_name) [Employee Name], join_date [Join Date] 
from Employee
join Biodata bio on biodata_id=bio.id

--NO 2. Menampilkan daftar karyawan yang saat ini sedang cuti. 
--Daftar berisi nomor_induk, nama, tanggal_mulai, lama_cuti dan keterangan.
select emp.nip nomor_induk, CONCAT(bio.first_name,' ',bio.last_name) [Employee Name], start_date tanggal_mulai, datediff(day,start_date,end_date) lama_cuti, reason keterangan
from Leave_Request lr
join Employee emp on emp.id=employee_id
join Biodata bio on bio.id = emp.biodata_id
group by emp.nip, CONCAT(bio.first_name,' ',bio.last_name), start_date, reason, end_date, emp.id

--NO 3. Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. 
---Tampilkan data berisi no_induk, nama, jumlah pengajuan .
select emp.nip nomor_induk, CONCAT(bio.first_name,' ',bio.last_name) [Employee Name], COUNT(lr.employee_id) [Jumlah pengajuan]
from Leave_Request lr
join Employee emp on emp.id=employee_id
join Biodata bio on bio.id = emp.biodata_id
group by emp.nip, bio.first_name, bio.last_name
having COUNT(lr.employee_id) > 2

--NO 4. Menampilkan sisa cuti tiap karyawan tahun ini, jika di ketahui jatah cuti setiap karyawan tahun ini adalah sesuai dengan quota cuti. 
--tampilan berisi no_induk, nama, quota, cuti yg sudah di ambil dan sisa_cuti
select emp.nip nomor_induk, CONCAT(bio.first_name,' ',bio.last_name) nama, 
	   el.regular_quota kuota_cuti, COUNT(lr.employee_id) cuti_yang_diambil, (el.regular_quota-COUNT(lr.employee_id)) sisa_cuti
from Employee emp
full outer join Employee_Leave el on emp.id = el.employee_id
full outer join Leave_Request lr on lr.employee_id = emp.id
full outer join Biodata bio on bio.id = emp.biodata_id
group by emp.nip, bio.first_name, bio.last_name, regular_quota

--NO 5. Perusahaan akan meberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 kali gaji. 
---Tampilan No induk, Fullname, Berapa lama bekerja, Bonus, Total Gaji(gaji + bonus)
select emp.nip, CONCAT(bio.first_name,' ',bio.last_name) Fullname, DATEDIFF(YEAR,emp.join_date, GETDATE()) lama_bekerja, (1.5*emp.salary) Bonus, emp.salary+(1.5*emp.salary) [Total Gaji]
from Employee emp
join Biodata bio on bio.id = emp.biodata_id
where DATEDIFF(YEAR,emp.join_date, GETDATE()) > 5

--NO 6. Tampilkan nip, nama_lengkap, jika karyawan ada yg berulang tahun di hari ini akan diberikan hadiah bonus sebanyak 5% dari gaji jika tidak ulang tahun maka bonus 0 dan total gaji . 
---Tampilkan No Induk, nama, Tgl lahir , Usia, Bonus, Total Gaji
select emp.nip, bio.first_name, bio.dob, DATEDIFF(year, bio.dob, cast(GETDATE() AS DATE)) Usia, (emp.salary*1.5) Bonus, emp.salary+(emp.salary*1.5) [Total gaji]
from Biodata bio
join Employee emp on bio.id = emp.biodata_id
where day(bio.dob) = day(cast(GETDATE() AS DATE)) and month(bio.dob) = month(cast(GETDATE() AS DATE))

---contoh lain
select emp.nip, bio.first_name, bio.dob, DATEDIFF(year, bio.dob,cast(GETDATE() AS DATE)) Usia, (emp.salary*1.5) Bonus, emp.salary+(emp.salary*1.5) [Total gaji]
from Biodata bio
join Employee emp on bio.id = emp.biodata_id
where day(bio.dob) = day(cast('2023/01/02' AS DATE)) and month(bio.dob) = month(cast('2023/01/02' AS DATE))

--NO 7. Tampilkan No Induk, nama, Tgl lahir , Usia. Urutkan biodata dari yg paling muda sampai yg tua
select emp.nip, bio.first_name, bio.dob, DATEDIFF(year, bio.dob,cast(GETDATE() AS DATE)) Usia
from Biodata bio
join Employee emp on bio.id = emp.biodata_id
order by DATEDIFF(year, bio.dob,cast(GETDATE() AS DATE)) asc

--NO 8. Tampikan Karyawan yg belum pernah Cuti
select bio.first_name [karyawan yang belum ambil cuti]
from Biodata bio
left join Employee emp on bio.id = emp.biodata_id
left join Employee_Leave el on emp.id = el.employee_id
left join Leave_Request lr on lr.employee_id = emp.id
where lr.employee_id is null

--NO 9. Tampikan Nama Lengkap, Jenis Cuti, Durasi Cuti, dan no telp yang sedang cuti
select CONCAT(first_name, ' ', last_name), l.type [jenis cuti], concat(DATEDIFF(DAY,lr.start_date,lr.end_date), ' hari')[Durasi cuti], cp.contact[no telp]
from Biodata bio
join Employee emp on bio.id = emp.biodata_id
join Leave_Request lr on lr.employee_id = emp.id
join Leave l on l.id = lr.leave_id
join Contact_Person cp on cp.biodata_id = bio.id
where cp.type = 'PHONE'

--NO 10. Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan
select bio.first_name
from Biodata bio 
left join Employee emp on emp.biodata_id=bio.id
where emp.nip is null



--NO 11. buatlah sebuah view yg menampilkan data nama lengkap, tgl lahir dan tmpat lahir , status, dan salary
create view vbiodata
as
first_name, last_name, dob, pob, salary
from Biodata bio
join Employee emp on emp.biodata_id = bio.id




--NO 12. Tampilkan  alasan cuti yang paling sering diajukan







